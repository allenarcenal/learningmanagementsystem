<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>

<script type='text/javascript' src="js/submitcomment.js"></script>


<s:iterator value="postListBean" var="postBeanLists" status="stat">
		<div class="bubble-list">
			<div class="bubble clearfix">
			    <img src="QQAssets/default_profile_image.png">
			    <div class="bubble-content">
			    <div class="point"></div>
					<p>${postBeanLists.name} posted <p>
					<p>${postBeanLists.content }</p>
<%-- 					<p>postid: ${postBeanLists.postID }</p> --%>
				</div>
			</div>
		</div>
		<s:iterator value="%{#postBeanLists.commentBeanList }" var="commentList">
		<div class="bubble-list-comment">
			<div class="bubble clearfix bubble-comment">
			    <img src="QQAssets/default_profile_image.png">
			    <div class="bubble-content">
			    <div class="point"></div>
					<p>${commentList.name } comments </p>
					<p>${commentList.commentContent }</p>
				</div>
			</div>
		</div>		
		</s:iterator>
		<br>
			<form method="post" id="submitComment">
				<input type="hidden" class="postid" value="${postBeanLists.postID }" name="postID"/>
				<div class="input-group">
				<input type="text" size = 30 name="commentContent" class="form-control" placeholder="Comment" id="content" required>
				<span class="input-group-btn">
				<input type="submit" class="btn btn-default" value="Send" />
				</span>
				</div>
			</form>
</s:iterator>
<br/>
<br/>
<br/>