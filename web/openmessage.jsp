<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
       <%@ taglib prefix="s" uri="/struts-tags" %>
       <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<title>Conversations</title>
</head>
<%@ include file="style/header.html"%>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
	<s:iterator value="messageContent" var="content">
	<div>
		<c:set var="myAccountID" value='<%= session.getAttribute("account_id").toString() %>'/>
		<c:choose>
			<c:when test="${myAccountID == sender}">
				  <div class="form-group">
				    <label for="name"><img src="QQAssets/default_profile_image.png" class="img-circle" height="40px" width="40px" /> You:</label>
				    <textarea class="form-control" rows="4" disabled> ${content} </textarea>
				    <c:set var="toSend" scope="session" value="${receiver}"/>
				  </div> 
			</c:when>
			<c:otherwise>
				  <div class="form-group">
				    <label for="name"><img src="QQAssets/default_profile_image.png" class="img-circle" height="40px" width="40px" /> ${other}:</label>
				    <textarea class="form-control" rows="4" disabled>${content}</textarea>
				    
				  </div> 
				  <c:set var="toSend" scope="session" value="${sender}"/>
      		</c:otherwise>  
		</c:choose>
<%-- 			<p>senderID: ${sender}	 </p> --%>
<%-- 			<p>receiverID: ${receiver} </p> --%>
<!-- 			<hr /> -->
<%-- 			<p>content: ${content} </p> --%>
		<br />
	</div>
	</s:iterator>
	${other} <!--  <c:out value="${toSend}"/> -->
	<form action="message.action" method="post">
		 <input type="hidden" name="msgType"  value="reply"/>
		 <input type="hidden" name="recName" value="${other}"/>
		 <input type="hidden" name="receiver"  value="<c:out value="${toSend}"/>"/>
		 <textarea class="form-control" rows="4" name="content" placeholder="Reply" required autofocus="autofocus" /></textarea>
		 <input class="btn btn-primary" type="submit" name="SEND" value="SEND"/>
	</form>
	
</body>
</html>