<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<title>Insert title here</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>Sender</th>
      <th>Content</th>
    </tr>
  </thead>
<s:iterator value="mbl" var="messages">
  <tbody>
<%--   	location.href='openMessageAct?messageID=${messages.messageID}' --%>
     <tr onclick="window.open('openMessageAct?messageID=<%= session.getAttribute("account_id").toString() %>&other=${messages.headerName}&qid=${messages.receiverID}', 'newwindow', 'width=666, height=347'); return false;" style="cursor: pointer;">
      <td> ${messages.headerName } </td>
      <s:iterator value="%{#messages.mcbl}" var="messageContent">
		<td>${messageContent.content } </td>
	  </s:iterator>
    </tr>   
  </tbody>
</s:iterator>
</table> 
</body>
</html>