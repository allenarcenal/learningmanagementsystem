 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<nav class="navbar navbar-default navbar-static-top">
			  <div class="container-fluid">
				    <div class="navbar-header">
				      <a class="navbar-brand" href="myprofile.jsp">iACADEMY</a>
				    </div>
				
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="myprofile.jsp"><span class="glyphicon glyphicon-user"></span> ${myUser.firstName} ${myUser.middleInitial}. ${myUser.lastName}</a></li>
				      	<li class="active"><a href="myprofile.jsp"><span class="glyphicon glyphicon-home"></span> Home<span class="sr-only">(current)</span></a></li>
				      
				        <li>
				        	<a href="message.jsp" onclick="window.open('message.jsp', 'newwindow', 'width=666, height=347'); return false;"><span class="glyphicon glyphicon-list"></span> <span class="glyphicon glyphicon-envelope"></span></a>
				        </li>
				        
				        <li>
				        	<a href="logout"> <span class="glyphicon glyphicon-cog"></span>Logout</a>
				        </li>
				        
				      </ul>
				    </div>
			  </div>
			</nav>
	</c:when> 
	<c:otherwise>
			<nav class="navbar navbar-default navbar-static-top">
			  <div class="container-fluid">
				    <div class="navbar-header">
				      <a class="navbar-brand" href="profhome.jsp">iACADEMY</a>
				    </div>
				
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav navbar-right">
				      	<li><a href="profhome.jsp"><span class="glyphicon glyphicon-user"></span> ${myUser.firstName} ${myUser.middleInitial}. ${myUser.lastName}</a></li>
				      	<li class="active"><a href="profhome.jsp"><span class="glyphicon glyphicon-home"></span> Home<span class="sr-only">(current)</span></a></li>
				      
				        <li>
				        	<a href="message.jsp" onclick="window.open('message.jsp', 'newwindow', 'width=666, height=347'); return false;"><span class="glyphicon glyphicon-list"></span> <span class="glyphicon glyphicon-envelope"></span></a>
				        </li>
				        
				        <li>
				        	<a href="logout"> <span class="glyphicon glyphicon-cog"></span>Logout</a>
				        </li>
				        
				      </ul>
				    </div>
			  </div>
			</nav>		
    </c:otherwise>   	
</c:choose>
	