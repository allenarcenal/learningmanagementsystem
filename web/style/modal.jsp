<!-- Modal -->
<script>
$(document).ready(function() {
	 $("#searchNames").tokenInput("searchAction");
   
});
</script>
				<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="exampleModalLabel">New message</h4>
				      </div>
				      <div class="modal-body">
				        <form action="message.action" method="post">
				          <div class="input-group">
				            <label for="searchNames" class="control-label">Recipient:</label>
				            <input class="form-control" type="text" id="searchNames" name="receiver" size="50" />
				          </div>
				          <div class="input-group">
				            <label for="messageContent" class="control-label">Message:</label>
				            <textarea class="form-control" cols="60" rows="10" name="messageContent"/></textarea>
				          </div>
				        </form>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary">Send message</button>
				      </div>
				    </div>
				  </div>
				</div><!-- Modal End -->