 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <c:choose>
	<c:when test="${myUser.role == 'student'}">
<div class="col-sm-3 col-md-2 affix-sidebar">
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<span class="visible-xs navbar-brand">iAc Sidebar</span>
					</div>
					<div class="navbar-collapse collapse sidebar-navbar-collapse">
						<div class="sidebar-back"></div>
						<ul class="nav navbar-nav" id="sidenav01">
							<li class="active"><a href="myprofile.jsp"> <img
									src="QQAssets/default_profile_image.png" class="img-circle"
									height="40px" width="40px" /> ${myUser.lastName}, ${myUser.firstName}
									${myUser.middleInitial} <br>
							</a></li>
							<li><a href="inbox.jsp"><span class="glyphicon glyphicon-envelope"></span>
									Inbox<span class="badge"><div id="notif"></div></span></a></li>								
							<li> <a href="changepassword.jsp"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
	</c:when> 
	<c:otherwise>
<div class="col-sm-3 col-md-2 affix-sidebar">
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<span class="visible-xs navbar-brand">iAc Sidebar</span>
					</div>
					<div class="navbar-collapse collapse sidebar-navbar-collapse">
						<div class="sidebar-back"></div>
						<ul class="nav navbar-nav" id="sidenav01">
							<li class="active"><a href="profhome.jsp"> <img
									src="QQAssets/default_profile_image.png" class="img-circle"
									height="40px" width="40px" /> ${myUser.lastName}, ${myUser.firstName}
									${myUser.middleInitial} <br>
							</a></li>
							<li><a href="inbox.jsp"><span class="glyphicon glyphicon-envelope"></span>
									Inbox <span class="badge"><div id="notif"></div></span></a></li>							
							<li><a href="#" data-toggle="collapse"
								data-target="#toggleRoom" data-parent="#sidenav01"
								class="collapsed"> <span class="glyphicon glyphicon-cloud"></span>
									Rooms <span class="caret pull-right"></span>
							</a>
										<div id="roomsUL"></div>
									</li>
							<li> <a href="changepassword.jsp"><span class="glyphicon glyphicon-lock"></span>Change Password</a></li>		
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
    </c:otherwise>   	
</c:choose>
