		<div class="col-sm-3 col-md-2 affix-sidebar">
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<span class="visible-xs navbar-brand">iAc Sidebar</span>
					</div>
					<div class="navbar-collapse collapse sidebar-navbar-collapse">
						<div class="sidebar-back"></div>
						<ul class="nav navbar-nav" id="sidenav01">
							<li class="active"><a href="admin.jsp"> <img
									src="QQAssets/default_profile_image.png" class="img-circle"
									height="40px" width="40px" /> Admin <br>
							</a></li>
							<li><a href="admin.action?stat=student"><span class="glyphicon glyphicon-lock"></span>
									Enroll students </a></li>
							<li><a href="admin.action?stat=prof"><span class="glyphicon glyphicon-lock"></span>
									Assign prof </a></li>
							<li><a href="register.jsp"><span class="glyphicon glyphicon-lock"></span>
									Register student </a></li>
							<li><a href="profRegister.jsp"><span class="glyphicon glyphicon-lock"></span>
									Register prof</a></li>
							<li><a href="insertSubject.jsp"><span class="glyphicon glyphicon-lock"></span>
									Create Subject</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div> 