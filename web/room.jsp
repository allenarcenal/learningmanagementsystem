<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<script src="js/sendpost.js"></script>
<script>
function loadPost(){
	$('#populatepost').load('populatepostAct');
}
</script>
<script>
	function upload(){
		alert("clicked");
		var formData = new FormData($('#fileForm'));
		$.ajax({
			url: 'upload',
			type: 'POST',
			xhr: function(){
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){
					myXhr.upload.addEventListener('progress',progressHandlingFunction,false);
				}
				return myXhr;
			},
			beforeSend: beforeSendHandler,
			success: completeHandler,
			error: errorHandler,
			data: formData,
			cache: false,
			contentType: false,
			processData: false
			
		});
	}
</script>


</head>
<body onLoad="loadPost()">

<!-- <div style="border:2px solid black">
	<form id="fileForm" enctype="multipart/form-data">
      <label for="myFile">Upload your file</label>
      <input id="fileUpload" type="file" name="myFile"/>
      <s:submit id="submitFile" onclick="upload()" value="Upload"/>
   </form>
   <progress></progress>
</div>  -->
<br />
<div id="postsendarea" style="border:2px solid black">
	<s:textarea id="submitPost" name="submitPost" placeholder="What's Happening" class="form-control"></s:textarea>
	<s:submit id="sendButton" label="Submit" onClick="sendPost()" ></s:submit>
</div>
   
   <div id="populatefiles">
   </div>
   
   <div id="populatepost">
   </div>
   
	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
   
   
</body>
</html>