<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Prof Sheet</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>  
	</c:choose>
<%@ taglib uri="http://www.zkoss.org/jsp/zul" prefix="z" %>
    <div width="100%" style="height: 100%;">
    	<z:page zscriptLanguage="java">
    		<z:window id="prof" title="ProfSheet">
    		 	<z:spreadsheet id="profSheet" apply="els.ss.handlers.CoeditController" width="100%" height="800px" maxrows="200" maxcolumns="40" showFormulabar="true" showToolbar="true" showSheetbar="true"></z:spreadsheet>
    		</z:window>
    		
        </z:page>
    </div>
</body>
</html>