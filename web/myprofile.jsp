<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script>
function loadtoken() {
	 $("#searchNames").tokenInput("searchAction");
}
</script>
<script>
	$(document).ready(function(event) {
		$('#rooms').load('populateroomAct')
		$('#roomsUL').load('populateroomActUL')
		$('#notif').load('notification');
	})
</script>
<title>My Profile</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when> 
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
	<%@ include file="style/usertopnav.jsp"%>

	<div class="row affix-row">
		<%@ include file="style/usersidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Home
					</h3>
				</div>
				Welcome, ${myUser.firstName}!
<!-- 				<div id="notif"></div> -->
				<hr />
				<div id=rooms></div>
				<%@ include file="style/modal.jsp"%>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>

	

	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>


</body>
</html>