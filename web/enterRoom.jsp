<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>

<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<script src="js/sendpost.js"></script>
<script>
	function loadPost() {
		$('#populatepost').load('populatepostAct');
	}
</script>
<script>
	$(document).ready(function(event) {
		$('#populatepost').load('populatepostAct');
		$('#roomsUL').load('populateroomActUL');
		$('#populatefiles').load('populateFiles');
		$('#populategrades').load('populateGrades');
		$('#notif').load('notification');
	})
</script>
<title>My Profile</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
	<%@ include file="style/usertopnav.jsp"%>
	<div class="row affix-row">
		<%@ include file="style/usersidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Room
					</h3>
				</div>
				<ul class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">Post</a></li>
					<li><a href="#profile" data-toggle="tab">Student Uploads</a></li>
					<li><a href="#grades" data-toggle="tab">Prof Uploads</a></li>
				</ul>
				<!-- Nav pill end -->
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade active in" id="home">
						<div id=rooms>
							<div id="postsendarea">
								<div class="form-group">
									<textarea class="form-control" rows="3" id="submitPost"
										placeholder="What's up?"></textarea>
								</div>
								<input type="submit" value="Submit" class="btn btn-primary"
									id="sendButton" onClick="sendPost()" style="float: right;">
								<br>
								<%--<s:submit id="sendButton" class="btn" label="Submit" onClick="sendPost()" ></s:submit> --%>
							</div>
						</div>
						<div id="populatepost"></div>
					</div>
					<div class="tab-pane fade" id="profile">
						<!-- upload -->
							<c:choose>	
							<c:when test="${myUser.role == 'student'}">
						<div style="border: 2px solid black">
							<form id="fileForm" action="upload" method="post"
								enctype="multipart/form-data" target="_parent">
								<label for="myFile">Upload your file</label> <input
									id="fileUpload" type="file" name="myFile" /> <input
									type="submit" value="Upload" />
							</form>

						</div>
							</c:when>
							</c:choose>
						<div id="populatefiles"></div>
					</div>
					<div class="tab-pane fade" id="grades">
						<!-- upload -->
							<c:choose>	
							<c:when test="${myUser.role == 'prof'}">
						<div style="border: 2px solid black">
							<form id="fileForm" action="upload" method="post"
								enctype="multipart/form-data" target="_parent">
								<label for="myFile">Upload your file</label> <input
									id="fileUpload" type="file" name="myFile" /> <input
									type="submit" value="Upload" />
							</form>
						</div>
							</c:when>
							</c:choose>
						<div id="populategrades"></div>
					</div>
				</div>
				<%@ include file="style/modal.jsp"%>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>



	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>


</body>
</html>