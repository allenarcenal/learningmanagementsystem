/* attach a submit handler to the form */
$(".sendcomment").click(function(event) {
	 /* stop form from submitting normally */
  event.preventDefault();
	var myform = $(this).closest("form");
	
	var content = $('#content').val();
	if(content == ''){
		alert("Empty textfield");
		return;
	}
	var postid = $('.postid').val();
	$.ajax({
		type:"POST",
		url: "sendcommentAct",
		data: myform.serialize(),
		success:function(){
			$('#populatepost').load('populatepostAct');
		}
	})
});