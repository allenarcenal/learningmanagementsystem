 $(function() {
    
            $('#search').autocomplete({
            source : function(request, response) {
            	
                    $.ajax({
                            url : "searchAction",
                            type : "POST",
                            data : {
                                    term : request.term
                            },
                            success : function(jsonResponse) {
                            	var list = {};
                            	var counter = 0;
                            	$.each(jsonResponse.searchBeanList,function(key,value){
                            		var name = "";
                                	
                            		$.each(value,function(key,value){
                            			if(key == "lastName"){
                            				name = name + value;
                            			}
                            			if(key == "firstName"){
                            				name = name + value + " ";
                            			}
                            		})

                    				list[counter] = name;
                    				counter++;
                            	})
                            	JSON.stringify(list);
                            	response(list);
                            	
                            }
                    });
                    }
            });
    });