$(document).ready(function(event){
	$.ajax({
        url: 'populatepostAct.action',
        type: 'POST',
        success: function(data){
        	
            // onSuccess take only the container content
            var content =  $($.parseHTML(data)); 
            //Replace content inside the div
            $('.postdiv2').html(content);
            // HIDE the overlay:
            $('#overlay').hide();
        }
    });
    // Create overlay and append to body:
    $('<div id="overlay"/>').css({
        top: 0,
        left: 0,
        width: '100%',
        height: $(window).height() + 'px',
        opacity:0.4, 
        background: 'lightgray url(http://bradsknutson.com/wp-content/uploads/2013/04/page-loader.gif) no-repeat center'
    }).hide().appendTo('body');
})
