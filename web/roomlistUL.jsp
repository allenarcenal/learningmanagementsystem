<%@ taglib prefix="s" uri="/struts-tags"%>

<div class="collapse" id="toggleRoom" style="height: 0px;">
	<ul class="nav nav-list">
		<s:iterator value="roomBeanList" var="room">
			<li><a href="enterroomAct?subject=${room.subjectID }&section=${room.subjectSection}">${room.subjectName}</a></li>
		</s:iterator>
	</ul>
</div>