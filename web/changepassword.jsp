<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script>
function loadtoken() {
	 $("#searchNames").tokenInput("searchAction");
}
</script>
<script>
	$(document).ready(function(event) {
		$('#rooms').load('populateroomAct')
		$('#roomsUL').load('populateroomActUL')
		$('#notif').load('notification');
	})
</script>
<title>My Profile</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
	<%@ include file="style/usertopnav.jsp"%>

	<div class="row affix-row">
		<%@ include file="style/usersidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Change Password
					</h3>
				</div>
				<form class="form-horizontal" action="changepass.action" method="post">
				  <fieldset>
				  	<div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
				  	<ul style="color:#FF0000"><s:fielderror name="error"/></ul>
					<div class="form-group">
	                    <label for="oldPass">Old Password</label>
	                    <div class="input-group">
	                        <input type="password" class="form-control" id="oldPass" name="oldPassword" size=10 required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
					<div class="form-group">
	                    <label for="newPass">New Password</label>
	                    <div class="input-group">
	                        <input type="password" class="form-control" id="newPass" name="password" size=10   required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
					<div class="form-group">
	                    <label for="confPass">Confirm New Password</label>
	                    <div class="input-group">
	                        <input type="password" class="form-control" id="confPass" name="confirmPassword" size=10 required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">          
				  </fieldset>
				</form>				
				<%@ include file="style/modal.jsp"%>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>

	

	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>


</body>
</html>
