<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="style/header.html"%>
<body>
<%@ include file="style/guestmenu.jsp"%>
	
 <div class="col-md-4"></div>
    <div class="col-md-4">
        <div class="container" style="margin-top: 30px">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1 class="panel-title"><strong><i class="fa fa-sign-in"></i> Sign In </strong></h1>
                    </div>
                    <div class="panel-body">
                        <form action="login.action" method="post">
                        	<ul class="error"><s:fielderror name="error"/></ul><br/>
                            <div class="form-group">
                                <label for="studentID"><i class="fa fa-user"></i> Student ID</label>
                                <input type="text" class="form-control" name="studentID" id="studentID" placeholder="Student ID" required="required" value="${studentID}"/>
                            </div>
                            <div class="form-group">
                                <label for="password"><i class="fa fa-key"></i> Password <a href="INSTRCTIONS FOR FORGET PASS HERE">(Forgot Password)</a></label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required"/>
                            </div>
                            <div class="form-group">
                            	 <label for="cptans"><img id="cptchaimg" class="LBD_CaptchaDiv" src="<c:url value="simpleCaptcha.png"/>"/> </label>
								<br/><br/>
								<input type="text" class="form-control" id="cptans" name="answer" placeholder="Captcha Image">       
                            </div>
                            <center><button type="submit" class="btn btn-sm btn-primary">Sign in</button></center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>