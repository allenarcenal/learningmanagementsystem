<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>iACADEMY-LearningManagementSystem</title>
</head>  
<link href='http://fonts.googleapis.com/css?family=Raleway:200,400,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/normalize.css" /> 
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/component.css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

<link href="https://fontastic.s3.amazonaws.com/iHUKqqXrXX6ZmbSgekUoTY/icons.css" rel="stylesheet">

<script src="js/modernizr.custom.js"></script>
<body>
	<s:if test="%{#session.get('myUser')!=null}">
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when> 
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>
	</s:if>
        <div class="container">
        	<div style="opacity:1;" class="topBG">
		        <div class="codrops-top clearfix">
					<a href="home.jsp" class="left"><i class="icon icon-logo-iac"></i> iACADEMY</a>
					<a href="#login" class="right"><i class="fa fa-sign-in"></i> Login</a>
				</div>
    		</div>
    		
	    	<div id="boxgallery" class="boxgallery" data-effect="effect-1">
				<div class="panel"><img src="images/1.jpg" alt="Image 1"/></div>
				<div class="panel"><img src="images/2.jpg" alt="Image 2"/></div>
				<div class="panel"><img src="images/3.jpg" alt="Image 3"/></div>					
				<div class="panel"><img src="images/4.jpg" alt="Image 4"/></div>
			</div>
		
			<header class="codrops-header">
				<h1>iACADEMY Learning Management System<span>Innovating Changes!</span></h1>
			</header>
			
        </div>
        
        
	<div id="login" class="modalDialog">
    	<div><a href="#close" title="Close" class="close">X</a>
    		<form action="login.action" method="post" class="elegant-aero">
    			<h1>iACADEMY 
       				 <span>Learning Management System</span>
    			</h1>
    			
    			<ul style="color:#FF0000"><s:fielderror name="error"/></ul>
    			
    			<label>
        			<span><i class="fa fa-user"></i> Account ID:</span>
        			<input type="text" name="userID" id="studentID" placeholder="Account ID" required="required" value="${studentID}"/>
    			</label>
    
			    <label>
			        <span><i class="fa fa-key"></i> Password:</span>
			        <input type="password" id="password" name="password" placeholder="Password" required="required"/>
			    </label>
			    
				<label>
					<span><i class="fa fa-puzzle-piece"></i> Captcha:</span>
					<input type="text" id="cptans" name="answer" placeholder="Captcha Image">
				</label>
			    
			    <label>
			    	<span></span>
			    	<img id="cptchaimg" class="LBD_CaptchaDiv" src="<c:url value="simpleCaptcha.png"/>"/><br/><br/>	
			    </label>
			    
			    <label>
			        <span>&nbsp;</span> 
			        <input type="submit" class="button" value="Sign In" /> 
   				</label>
			    
			</form>
   	 	</div>
	</div>
    
    <script src="js/classie.js"></script>
	<script src="js/boxesFx.js"></script>
	<script>
		new BoxesFx( document.getElementById( 'boxgallery' ) );
	</script>
	
</body>
</html>