<div id="sequence">
		<img class="sequence-prev" src="images/bt-prev.png" alt="Previous" />
		<img class="sequence-next" src="images/bt-next.png" alt="Next" />

		<ul class="sequence-canvas">
			<li class="animate-in">
				<div class="info">
					<!-- <h2>Welcome to Milamba's Australia</h2>
					<p>What you'll find here are the things I love about my
						country.</p> -->
				</div> <img class="sky" src="images/carousel3.png" alt="Blue Sky" /> <img
				class="balloon" src="images/kangaroo.png" alt="Snake" />
			</li>
			<li>
				<div class="info">
					<!-- <h2>Australia like no other</h2>
					<p>Enjoy photographs of places, people, incredible flora and
						fauna and growing list of Honorary Aussies.</p> -->
				</div> <img class="sky" src="images/carousel1.png" alt="Blue Sky" /> <img
				class="aeroplane" src="images/parrot.png" alt="Parrot" />
			</li>
			<li>
				<div class="info">
					<!-- <h2>Roll in</h2>
					<p>Scan, Share, Appreciate the site. Have Fun!</p> -->
				</div> <img class="sky" src="images/carousel2.png" alt="Blue Sky" /> <img
				class="kite" src="images/butterfly.png" alt="Kite" />
			</li>
		</ul>
	</div>