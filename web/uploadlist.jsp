<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when>
	</c:choose>

<div style="height:350px; width:1000px;overflow-y:auto;overflow-x: hidden;">
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>File Name</th>
      <th>Uploader</th>
      <th>Upload Date</th>
      <th>Options</th>
    </tr>
  </thead>
<s:iterator value="uploadBeanList" var="files">
  <tbody>
	<c:choose>	
	<c:when test="${files.role == 'student'}">
    <tr>
      <td>
      <form action="download.html" method="post">
      <a href="#"> ${files.myFileFileName} </a>
      <input type="hidden" name="filePath" value="${files.myFileFileName}"/> 
      </form>
      <c:set var="msg" value="${files.myFileFileName}"/>
	  <c:set var="arrayofmsg" value="${fn:split(msg,'.')}"/>
      </td>	
      <td>${files.firstName} ${files.lastName } </td>
      <td>${files.myDate}</td>
      <td><c:choose>
			   <c:when test="${arrayofmsg[1] == 'xlsx'}">
			   	  <form action='download.html' method='post'>
			      <a href='javascript:;' onclick='parentNode.submit();'> Download </a>
			      <input type='hidden' name='action' value='download'/>
			      <input type='hidden' name='myName' value='${files.firstName} ${files.lastName }'/>
			      <input type='hidden' name='filePath' value='${files.myFileFileName}'/> 
			      </form>
			      <form action="download.html" method="post" target="_blank">
			      <a href='javascript:;' onclick='parentNode.submit();'> View </a>
			      <input type='hidden' name='action' value='view'/>  
			      <input type='hidden' name='myName' value='${files.firstName} ${files.lastName }'/>
			      <input type='hidden' name='filePath' value='${files.myFileFileName}'/> 
			      <input type='hidden' name='role' value='${myUser.role}'/>
			      </form>
			   </c:when> 
			   <c:otherwise>
			   	  <form action='download.html' method='post'>
			      <a href='javascript:;' onclick='parentNode.submit();'> Download </a>
			      <input type='hidden' name='action' value='download'/> 
			      <input type='hidden' name='myName' value='${files.firstName} ${files.lastName }'/>
			      <input type='hidden' name='filePath' value='${files.myFileFileName}'/> 
			      </form>
      		   </c:otherwise>   
		  </c:choose>
	  </td>
    </tr>
    	</c:when> 
     </c:choose>
  </tbody>
</s:iterator>
</table> 
</div>