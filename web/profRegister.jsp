<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<title>iAcademy - e-Learning</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when>  
	</c:choose>
	<%@ include file="style/admintopnav.jsp"%>
	<div class="row affix-row">
		<%@ include file="style/adminsidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">
				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Register Prof
					</h3>
				</div>
				<div style="padding:0 50px;height:480px; width:1000px;overflow-y:auto;overflow-x: hidden;float:left;">		
				<form class="form-horizontal" action="profRegister.action" method="post">
				  <fieldset>
				  	<div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
				  	<ul style="color:#FF0000"><s:fielderror name="error"/></ul>
                <div class="form-group">
                    <label for="lastName">Last Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="lastName" name="lastName" value="${lastName}" placeholder="Last Name" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstName">First Name</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="firstName" name="firstName" value="${firstName}" placeholder="First Name" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="middleInitial">Middle Initial</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="middleInitial" name="middleInitial" value="${middleName}"placeholder="Middle Initial" maxlength="2">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="profID">Professor ID</label>
                    <div class="input-group">
                       <input type="text" class="form-control" name="profID" value="${profID}"placeholder="Professor ID" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="password" placeholder="********" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmPassword">Confirm Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="confirmPassword" placeholder="********" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cptans"><img id="cptchaimg" class="LBD_CaptchaDiv" src="<c:url value="simpleCaptcha.png"/>"/></label>
                    <div class="input-group">
                      	 <input type="text" class="form-control" id="cptans" name="answer" placeholder="Captcha Image">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div> 
	                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">          
				  </fieldset>
				</form>
				</div>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>
</body>
</html>
        <form action="profRegister.action" method="post">
            <div class="col-lg-6">
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
          
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
            </div>
        </form>