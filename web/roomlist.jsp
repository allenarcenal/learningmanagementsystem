<%@ taglib prefix="s" uri="/struts-tags"%>

<%-- <s:iterator value="roomBeanList" var="roomBeanLists"> --%>
<%-- 	<p><a href="enterroomAct?subject=${roomBeanLists.subjectID }&section=${roomBeanLists.subjectSection}">${roomBeanLists.subjectName} </a></p> --%>
<%-- </s:iterator> --%>
<script>
$('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
});
</script>
<table class="table table-striped table-hover">
	<thead>
		<tr class="warning">
			<th>Section</th>
			<th>Subject Name</th>
			<th>Day</th>
			<th>Time</th>
			<th>Room</th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="roomBeanList" var="room">
			<tr data-href="enterroomAct?subject=${room.subjectID }&section=${room.subjectSection}" class="info">
				<td>${room.subjectSection}</td>
				<td>${room.subjectName}</td>
				<td>${room.subjectDay}</td>
				<td>${room.subjectTime}</td>
				<td>${room.subjectRoom}</td>
			</tr>
		</s:iterator>
	</tbody>
</table>
<br />
<p>Room Archives</p>
<table class="table table-striped table-hover">
	<thead>
		<tr class="warning">
			<th>Section</th>
			<th>Subject Name</th>
			<th>Day</th>
			<th>Time</th>
			<th>Room</th>
			<th>School Year</th>
			<th>Term</th>
		</tr>
	</thead>
	<tbody>
		<s:iterator value="archiveRoomBeanList" var="room">
			<tr data-href="enterroomAct?subject=${room.subjectID }&section=${room.subjectSection}" class="info">
				<td>${room.subjectSection}</td>
				<td>${room.subjectName}</td>
				<td>${room.subjectDay}</td>
				<td>${room.subjectTime}</td>
				<td>${room.subjectRoom}</td>
				<td>${room.schoolYear }</td>
				<td>${room.term }</td>
			</tr>
		</s:iterator>
	</tbody>
</table>