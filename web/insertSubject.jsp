<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<title>iAcademy - e-Learning</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when>  
	</c:choose>
	<%@ include file="style/admintopnav.jsp"%>
	
		<div class="row affix-row">
		<%@ include file="style/adminsidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">
				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Create Subject
					</h3>
				</div>
				<div style="padding:0 50px;height:480px; width:1000px;overflow-y:auto;overflow-x: hidden;float:left;">		
				<form class="form-horizontal" action="createSubject.action" method="post">
				  <fieldset>
				  	<div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>Required Field</strong></div>
				  	<ul style="color:#FF0000"><s:fielderror name="error"/></ul>
					<div class="form-group">
	                    <label for="courseID">Course ID</label>
	                    <div class="input-group">
	                      	 <select class="form-control" name="courseID" placeholder="Course ID" required>
							 	<option>BSCS-SE</option>
							 </select>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
					<div class="form-group">
	                    <label for="subjectID">Subject ID</label>
	                    <div class="input-group">
	                        <input type="text" class="form-control" id="subjectID" name="subjectID" value="${subjectID}" placeholder="Subject ID" required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                <div class="form-group">
	                    <label for="subjectName">Subject Name</label>
	                    <div class="input-group">
	                        <input type="text" class="form-control" id="subjectName" name="subjectName" value="${subjectName}" placeholder="Subject Name" required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label for="subjectTime">Time</label>
	                    <div class="input-group">
	                        <input type="text" class="form-control" id="subjectTime" name="subjectTime" value="${subjectTime}"placeholder="Time" required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label for="subjectDays">Day</label>
	                    <div class="input-group">
	                         <input type="text" class="form-control" name="subjectDays" value="${subjectDays}" placeholder="Day (M,T,W,TH,F,S)" maxlength="2" required> 
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                          
	                <div class="form-group">
	                    <label for="yearLevel">Year Level</label>
	                    <div class="input-group">
							<select class="form-control" id="yearLevel" name="yearLevel">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
							</select>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                               
	                <div class="form-group">
	                    <label for="section">Section</label>
	                    <div class="input-group">
	                       <input type="text" class="form-control" name="section" value="${section}"placeholder="Section" required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                
	                <div class="form-group">
	                    <label for="roomNumber">Room Number</label>
	                    <div class="input-group">
	                        <input type="text" class="form-control" name="roomNumber" placeholder="Room Number" required>
	                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
	                    </div>
	                </div>
	                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">          
				  </fieldset>
				</form>
				</div>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>
	

</body>
</html>