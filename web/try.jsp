<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<%@ include file="style/header.html"%>
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
	  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">iACADEMY</a>
		    </div>
		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav">
		      	<li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i></a>
			        	<ul class="dropdown-menu">
				            <li><a href="#">No messages Received yet.</a></li>
			          </ul>
		        </li>
		      </ul>
		    
		      <ul class="nav navbar-nav navbar-right">
		      	<li><a href="#"><i class="fa fa-user"></i> Kim Orobia</a></li>
		      	<li class="active"><a href="#">Home<span class="sr-only">(current)</span></a></li>
		      
		        <li class="dropdown">
		        	<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i></a>
			        	<ul class="dropdown-menu">
				            <li><a href="#">No messages Received yet.</a></li>
			          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> <i class="fa fa-bars"></i> <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">Account Settings</a></li>
		            <li><a href="#">My Courses</a></li>
		            <li><a href="#">Betlog ni Ron</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Help</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Logout</a></li>
		          </ul>
		        </li>
		        
		      </ul>
		    </div>
	  </div>
	</nav>
	
	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	
</body>
</html>