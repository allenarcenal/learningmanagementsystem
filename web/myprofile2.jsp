<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script>
function loadtoken() {
	 $("#searchNames").tokenInput("searchAction");
}
</script>
<script>
	$(document).ready(function(event) {
		$('#rooms').load('populateroomAct')
		$('#roomsUL').load('populateroomActUL')
		$("#searchNames").tokenInput("searchAction");
	})
</script>
<title>My Profile</title>
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
	  <div class="container-fluid">
		    <div class="navbar-header">
		      <a class="navbar-brand" href="#">iACADEMY</a>
		    </div>
		
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right">
		      	<li><a href="myprofile.jsp"><span class="glyphicon glyphicon-user"></span> ${myUser.firstName} ${myUser.middleInitial}. ${myUser.lastName}</a></li>
		      	<li class="active"><a href="myprofile.jsp"><span class="glyphicon glyphicon-home"></span> Home<span class="sr-only">(current)</span></a></li>
		      
		        <li>
		        	<a href="message.jsp"><span class="glyphicon glyphicon-list"></span> <span class="glyphicon glyphicon-envelope"></span></a>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <span class="glyphicon glyphicon-align-justify"></span> <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="#">Account Settings</a></li>
		            <li><a href="#">My Courses</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Help</a></li>
		            <li class="divider"></li>
		            <li><a href="#">Logout</a></li>
		          </ul>
		        </li>
		        
		      </ul>
		    </div>
	  </div>
	</nav>

	<div class="row affix-row">
		<div class="col-sm-3 col-md-2 affix-sidebar">
			<div class="sidebar-nav">
				<div class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
							data-target=".sidebar-navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<span class="visible-xs navbar-brand">iAc Sidebar</span>
					</div>
					<div class="navbar-collapse collapse sidebar-navbar-collapse">
						<div class="sidebar-back"></div>
						<ul class="nav navbar-nav" id="sidenav01">
							<li class="active"><a href="myprofile.jsp"> <img
									src="QQAssets/default_profile_image.png" class="img-circle"
									height="40px" width="40px" /> ${myUser.lastName}, ${myUser.firstName}
									${myUser.middleInitial} <br>
							</a></li>
							<li><a href="#" data-toggle="collapse"
								data-target="#toggleRoom" data-parent="#sidenav01"
								class="collapsed"> <span class="glyphicon glyphicon-cloud"></span>
									Rooms <span class="caret pull-right"></span>
							</a>
										<div id="roomsUL"></div>
									</li>
							<li><a href="#"><span class="glyphicon glyphicon-lock"></span>
									Classes </a></li>
							<li><a href="#"><span class="glyphicon glyphicon-cog"></span>
									Options</a></li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Home
					</h3>
				</div>
				Welcome, ${myUser.firstName}!
				<hr />
				<div id=rooms></div>
				<%@ include file="style/modal.jsp"%>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>

	

	<script type="text/javascript" src="assets/js/jquery-min.js"></script>
	<script src="http://code.jquery.com/jquery.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>


</body>
</html>