<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<%@ include file="style/header.html"%>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<title>iAcademy - e-Learning</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when>  
	</c:choose>
	<%@ include file="style/admintopnav.jsp"%>

	<div class="row affix-row">
		<%@ include file="style/adminsidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Home
					</h3>
				</div>
				Welcome, Admin! What do you want to do today?
				<hr />
				<div class="alert alert-dismissible alert-success">
				  <button type="button" class="close" data-dismiss="alert">�</button>
				  <font color="black">You have successfully registered an account.</font>
				</div>
				<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Actions</h4></div>
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="admin.action?stat=student" class="list-group-item">Enroll students</a>
                                <a href="admin.action?stat=prof" class="list-group-item">Assign prof</a>
                                <a href="register.jsp" class="list-group-item">Register student</a>
                                <a href="profRegister.jsp" class="list-group-item">Register prof</a>
                                <a href="insertSubject.jsp" class="list-group-item">Create Subject</a>
                            </div>
                        </div>
                </div>
				<%@ include file="style/footer.html"%>
			</div>
		</div>
	</div>
</body>
</html>