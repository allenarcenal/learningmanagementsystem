<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ include file="style/header.html"%>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when>  
	</c:choose>
	<%@ include file="style/admintopnav.jsp"%>

	<div class="row affix-row">
		<%@ include file="style/adminsidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Subject Info
					</h3>
				</div>
				<form action="studentToSubject.action" method="post">
					SUBJECT ID: <input type="hidden" name="subjectID" value="${adminBean.subject_id}">${adminBean.subject_id}<br/>
					COURSE ID: <input type="hidden" name="courseID" value="${adminBean.course_id}">${adminBean.course_id}<br/>
					YEAR: <input type="hidden" name="year" value="${adminBean.year }">${adminBean.year }<br/><br/>
					SECTION: <input type="hidden" name=section value="${adminBean.section }">${adminBean.section }<br /><br />
					<h4>List of Students</h4>
<!-- 					<div style="height:270px; overflow: auto;"> -->
					<table class="table table-striped display" id="myTable" width="100%">
						<thead>
							<tr class="warning">
								<th>Option</th>
								<th>Student ID</th>
								<th>Student Name</th>
								<th>Student Year Level</th>
							</tr>
						</thead>
						<tbody>
					<s:iterator value="studentListBean" var= "studentList">
							<tr>
							<td><input type="checkbox" name="studentID" value="<s:property value="#studentList.studentID"/>"></td>
							<td><s:property value="#studentList.studentID"/></td>
							<td><s:property value="#studentList.firstName"/> <s:property value="#studentList.middleInitial"/> <s:property value="#studentList.lastName"/></td>  
							<td><s:property value="#studentList.yearLevel"/></td>
							</tr>
						
					</s:iterator>
						</tbody>
						</table>
<!-- 					</div> -->
					<button type="submit" class="btn btn-sm btn-primary">Submit</button>
				</form>
			</div>
				<br/>
				<br/>
				<br/>
		</div>
	</div>
</body>
<script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>
</html>