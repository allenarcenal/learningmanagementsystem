<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="style/header.html"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>iAcademy - e-Learning</title>
</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'student'}">
			<c:redirect url="myprofile.jsp"></c:redirect>
	</c:when>
	<c:when test="${myUser.role == 'prof'}">
			<c:redirect url="profhome.jsp"></c:redirect>
	</c:when>  
	</c:choose>
	<%@ include file="style/admintopnav.jsp"%>

	<div class="row affix-row">
	<%@ include file="style/adminsidebar.jsp"%>
		<div class="col-sm-9 col-md-10 affix-content">
			<div class="container">

				<div class="page-header">
					<h3>
						<span class="glyphicon glyphicon-home"></span> Select your Subject
					</h3>
				</div>
						
						<form class="form-horizontal" action="enrollstat.action" method="post">
<!-- 							<div style="float:left;padding:0 50px 0 0"> -->
<!-- 							<h4>SUBJECT ID</h4> -->
<%-- 							<s:iterator value="adminListBean" var="admin"> --%>
<%-- 								<input type="radio" name="adminBean.subject_id" value="<s:property value="#admin.subject_id"/>"><s:property value="#admin.subject_id"/><br/> --%>
<%-- 							</s:iterator> --%>
<!-- 							</div> -->
<!-- 							<div style="float:left;padding:0 50px 0 0"> -->
<!-- 							<h4>COURSE ID</h4> -->
<%-- 							<s:iterator value="adminListBean" var="admin"> --%>
<%-- 								<input type="radio" name="adminBean.course_id" value="<s:property value="#admin.course_id"/>"><s:property value="#admin.course_id"/><br/> --%>
<%-- 							</s:iterator> --%>
<!-- 							</div> -->
<!-- 							<div style="float:left;padding:0 50px 0 0"> -->
<!-- 							<h4>Year</h4> -->
<%-- 							<s:iterator value="adminListBean" var="admin"> --%>
<%-- 								<input type="radio" name="adminBean.year" value="<s:property value="#admin.year"/>"><s:property value="#admin.year"/><br/> --%>
<%-- 							</s:iterator> --%>
<!-- 							</div> -->
<!-- 							<div style="float:left;"> -->
<!-- 							<h4>Section</h4> -->
<%-- 							<s:iterator value="adminListBean" var="admin"> --%>
<%-- 								<input type="radio" name="adminBean.section" value="<s:property value="#admin.section"/>"><s:property value="#admin.section"/><br/> --%>
<%-- 							</s:iterator> --%>
<!-- 							</div> -->
<!-- 							<div style="float:left;position:fixed;top:350px"> -->
<!-- 							<button type="submit" class="btn btn-sm btn-primary">Sumbit</button> -->
<!-- 							</div> -->
							
	                    <div class="panel panel-default">
<!-- 	                        <div class="panel-heading"> -->
<!-- 	                            <h4>New Requests</h4></div> -->
	                        <div class="panel-body">
	                         <h4>Subject ID</h4>
							 <select class="form-control" name="adminBean.subject_id" placeholder="Subject ID" required>
							   <s:iterator value="adminListBean" var="admin">
							 	  <option value="<s:property value="#admin.subject_id"/>"><s:property value="#admin.subject_id"/></option>
							   </s:iterator>
							 </select>
							 <br><h4>Course</h4>
							 <select class="form-control" name="adminBean.course_id" placeholder="Course" required>
<%-- 							   <s:iterator value="adminListBean" var="admin"> --%>
<%-- 							 	  <option value="<s:property value="#admin.course_id"/>"><s:property value="#admin.course_id"/></option> --%>
<%-- 							   </s:iterator> --%>
								   <option value="BSCS-SE">BSCS-SE</option>
							 </select>
							 <br><h4>Year</h4>
							 <select class="form-control" name="adminBean.year" placeholder="Year" required>
<%-- 							   <s:iterator value="adminListBean" var="admin"> --%>
<%-- 							 	  <option value="<s:property value="#admin.year"/>"><s:property value="#admin.year"/></option> --%>
<%-- 							   </s:iterator> --%>
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
							 </select>
							 <br><h4>Section</h4>
							 <select class="form-control" name="adminBean.section" placeholder="Section" required>
<%-- 							   <s:iterator value="adminListBean" var="admin"> --%>
<%-- 							 	  <option value="<s:property value="#admin.section"/>"><s:property value="#admin.section"/></option> --%>
<%-- 							   </s:iterator> --%>
								   <option value="SE11">SE11</option>
								   <option value="SE21">SE21</option>
								   <option value="SE31">SE31</option>
								   <option value="SE41">SE41</option>
							 </select>			
							 <br>					
							<button type="submit" class="btn btn-sm btn-primary">Submit</button>
	                    </div>	
	                    </div>
	                    <br>						
						</form>	
						<br>
			</div>
		</div>
	</div>


</body>
</html>