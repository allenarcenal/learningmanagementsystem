<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/main.css" rel="stylesheet"> 
<link href="assets/css/bootstrap-theme.css" rel="stylesheet">
<script src="assets/js/bootstrap.js"> </script>
<link rel="stylesheet" type="text/css" href="assets/css/style.css" />
<link rel="stylesheet" type="text/css" href="css/profile.css" />
<link rel="stylesheet" type="text/css" href="QQAssets/sidebar.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.tokeninput.js"></script>

<link rel="stylesheet" type="text/css" href="css/token-input-facebook.css" />
<title>Sending a Message</title>

<script src="js/jquery-1.11.2.js" > </script>

<script src="js/jquery.tokeninput.js"></script>
<script>
$(document).ready(function() {
	 $("#searchNames").tokenInput("searchAction");
   
});
</script>

</head>
<body>
	<s:if test="%{#session.get('myUser')==null}">
			<c:redirect url="home.jsp"></c:redirect>
	</s:if>	
	<c:choose>
	<c:when test="${myUser.role == 'admin'}">
			<c:redirect url="admin.jsp"></c:redirect>
	</c:when> 
	</c:choose>

	<form action="message.action" method="post">
		Recipient: <input type="text" name="receiver" size="65" id="searchNames" placeholder="Receipient" required/>
		<input type="hidden" name="msgType"  value="send"/>
		<br>
		<textarea cols="70" rows="10" name="content" placeholder="Message" required/></textarea>
		<input class="btn btn-primary" type="submit" name="SEND" value="SEND"/>
	</form>
	
<!--  	 <script type="text/javascript">
        $(document).ready(function() {
        	
            $("#messageReceiver").tokenInput([
                {id: 7, name: "Ruby"},
                {id: 11, name: "Python"},
                {id: 13, name: "JavaScript"},
                {id: 17, name: "ActionScript"},
                {id: 19, name: "Scheme"},
                {id: 23, name: "Lisp"},
                {id: 29, name: "C#"},
                {id: 31, name: "Fortran"},
                {id: 37, name: "Visual Basic"},
                {id: 41, name: "C"},
                {id: 43, name: "C++"},
                {id: 47, name: "Java"}
            ]);
        });
     </script> -->

</body>
</html>