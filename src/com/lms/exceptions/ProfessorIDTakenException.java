package com.lms.exceptions;

public class ProfessorIDTakenException extends Exception implements Message{
	private static final long serialVersionUID = 1L;
	
	public ProfessorIDTakenException(){
		super(PROF_ID_TAKEN);
	}
	
	public ProfessorIDTakenException(String message){
		super(message);
	}
}
