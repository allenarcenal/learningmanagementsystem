package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.PostBean;

public class PostBeanList {
	private LinkedList<PostBean> postListBean;

	public LinkedList<PostBean> getPostListBean() {
		return postListBean;
	}

	public void setPostListBean(LinkedList<PostBean> postListBean) {
		this.postListBean = postListBean;
	}
}
