package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.ProfessorBean;

public class ProfBeanList {
	private LinkedList<ProfessorBean> profList;

	public LinkedList<ProfessorBean> getProfList() {
		return profList;
	}

	public void setProfList(LinkedList<ProfessorBean> profList) {
		this.profList = profList;
	}
}
