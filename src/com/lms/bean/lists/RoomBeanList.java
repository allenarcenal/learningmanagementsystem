package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.RoomBean;

public class RoomBeanList {

	private LinkedList<RoomBean> roomBeanList;
	private LinkedList<RoomBean> archiveRoomBeanList;
	
	public LinkedList<RoomBean> getRoomBeanList() {
		return roomBeanList;
	}

	public void setRoomBeanList(LinkedList<RoomBean> roomBeanList) {
		this.roomBeanList = roomBeanList;
	}

	public LinkedList<RoomBean> getArchiveRoomBeanList() {
		return archiveRoomBeanList;
	}

	public void setArchiveRoomBeanList(LinkedList<RoomBean> archiveRoomBeanList) {
		this.archiveRoomBeanList = archiveRoomBeanList;
	}

	
}
