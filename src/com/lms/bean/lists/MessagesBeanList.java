package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.MessagesBean;

public class MessagesBeanList {
	private LinkedList<MessagesBean> mbl;

	public LinkedList<MessagesBean> getMbl() {
		return mbl;
	}

	public void setMbl(LinkedList<MessagesBean> mbl) {
		this.mbl = mbl;
	}
}
