package com.lms.bean.lists;

import com.lms.model.bean.CommentBean;

import java.util.LinkedList;

public class CommentBeanList {
	private LinkedList<CommentBean> commentBeanList;

	public LinkedList<CommentBean> getCommentBeanList() {
		return commentBeanList;
	}

	public void setCommentBeanList(LinkedList<CommentBean> commentBeanList) {
		this.commentBeanList = commentBeanList;
	}
	
	
}
