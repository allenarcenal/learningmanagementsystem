package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.StudentInfoBean;

public class StudentBeanList {
	private LinkedList<StudentInfoBean> studentListBean;
	

	public LinkedList<StudentInfoBean> getStudentListBean() {
		return studentListBean;
	}

	public void setStudentListBean(LinkedList<StudentInfoBean> studentListBean) {
		this.studentListBean = studentListBean;
	}
	

}
