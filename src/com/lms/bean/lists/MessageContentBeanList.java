package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.MessageContentBean;

public class MessageContentBeanList {
	private LinkedList<MessageContentBean> messageContent;

	public LinkedList<MessageContentBean> getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(LinkedList<MessageContentBean> messageContent) {
		this.messageContent = messageContent;
	}
}
