package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.AdminBean;

public class AdminBeanList {
	private LinkedList<AdminBean> adminListBean;

	public LinkedList<AdminBean> getAdminListBean() {
		return adminListBean;
	}

	public void setAdminListBean(LinkedList<AdminBean> adminListBean) {
		this.adminListBean = adminListBean;
	}

}
