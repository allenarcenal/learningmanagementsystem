package com.lms.bean.lists;

import java.util.LinkedList;

import com.lms.model.bean.UploadBean;

public class UploadBeanList {
	private LinkedList<UploadBean> uploadBeanList;

	public LinkedList<UploadBean> getUploadBeanList() {
		return uploadBeanList;
	}

	public void setUploadBeanList(LinkedList<UploadBean> uploadBeanList) {
		this.uploadBeanList = uploadBeanList;
	}
}
