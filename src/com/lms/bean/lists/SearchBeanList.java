package com.lms.bean.lists;

import java.io.Serializable;
import java.util.ArrayList;

import com.lms.model.bean.SearchBean;
import com.lms.model.bean.UserAccountBean;

public class SearchBeanList implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//private ArrayList<UserAccountBean> searchBeanList;
	private ArrayList<SearchBean> searchList;

//	public ArrayList<UserAccountBean> getSearchBeanList() {
//		return searchBeanList;
//	}
//
//	public void setSearchBeanList(ArrayList<UserAccountBean> searchBeanList) {
//		this.searchBeanList = searchBeanList;
//	}
//
	public ArrayList<SearchBean> getSearchList() {
		return searchList;
	}

	public void setSearchList(ArrayList<SearchBean> searchList) {
		this.searchList = searchList;
	}
}
