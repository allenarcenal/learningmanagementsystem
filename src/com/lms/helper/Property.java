package com.lms.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {
		
	public String getPropValue(String key) throws IOException{
		String value = "";
		
		Properties prop = new Properties();
		String propFileName = "configurations.property";
		
		InputStream input = getClass().getClassLoader().getResourceAsStream(propFileName);
		prop.load(input);
		if(input == null){
			throw new FileNotFoundException("property file " + propFileName + " not found");
		}
		
		value  = prop.getProperty(key);
		
		return value;
	}
	
}
