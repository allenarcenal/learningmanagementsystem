package com.lms.helper;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Map;

import com.lms.exceptions.PasswordNotMatchException;
import com.lms.exceptions.ProfessorIDTakenException;
import com.lms.exceptions.StudentIDTakenException;
import com.lms.model.action.LoginAction;
import com.lms.model.bean.AddressBean;
import com.lms.model.bean.AdminBean;
import com.lms.model.bean.CommentBean;
import com.lms.model.bean.MessageBean;
import com.lms.model.bean.MessageContentBean;
import com.lms.model.bean.MessagesBean;
import com.lms.model.bean.PostBean;
import com.lms.model.bean.ProfToSubjectBean;
import com.lms.model.bean.ProfessorBean;
import com.lms.model.bean.RoomBean;
import com.lms.model.bean.SearchBean;
import com.lms.model.bean.StudentBean;
import com.lms.model.bean.StudentInfoBean;
import com.lms.model.bean.StudentToSubjectBean;
import com.lms.model.bean.SubjectBean;
import com.lms.model.bean.UploadBean;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.SQL;
import com.lms.utils.MD5;

public class DatabaseBusinessDelegate implements SQL{
	
//	public static DatabaseBusinessDelegate getInstance(){
//		if (dbg == null){
//			dbg = new DatabaseBusinessDelegate();
//		}
//		return dbg;
//	}
//	
	public void validateRegister(Connection conn, UserAccountBean student) throws PasswordNotMatchException, StudentIDTakenException{
		if(!student.getPassword().equals(student.getConfirmPassword())){
			throw new PasswordNotMatchException();
		} else if(isStudentIDExist(conn, student.getStudentID())){
			throw new StudentIDTakenException();
		}
	}
	
	public void validateProfRegister(Connection conn, UserAccountBean prof) throws PasswordNotMatchException, ProfessorIDTakenException{
		if(!prof.getPassword().equals(prof.getConfirmPassword())){
			throw new PasswordNotMatchException();
		} else if(isProfIDExist(conn, prof.getProfID())){
			throw new ProfessorIDTakenException();
		}
	}
	
	private boolean isProfIDExist(Connection conn, String profID) {
		boolean exist = false;
		try {
			Statement s = conn.createStatement();
			ResultSet rs = null;
			String user = "";
			String stmt = "SELECT prof_id FROM professor";
			rs = s.executeQuery(stmt);
			while (rs.next()) {
				user = rs.getString("prof_id");
				if (user.equals(profID)) {
					exist = true;
					break;
				} else {
					exist = false;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return exist;
	}
	
	private boolean isStudentIDExist(Connection conn, String studentID) {
		boolean exist = false;
		try {
			Statement s = conn.createStatement();
			ResultSet rs = null;
			String user = "";
			String stmt = "SELECT student_id FROM students";
			rs = s.executeQuery(stmt);
			while (rs.next()) {
				user = rs.getString("student_id");
				if (user.equals(studentID)) {
					exist = true;
					break;
				} else {
					exist = false;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return exist;
	}
	
	public int insertNewStudent(Connection conn, UserAccountBean student) {
		int accID = 0;
		PreparedStatement ps = null;
		String role = "student";
		try {
			ps = conn.prepareStatement(INSERT_NEW_USER_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, student.getPassword());
			ps.setString(2, student.getSalt());
			ps.setString(3, role);
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			 if(rs.next()){
				 accID = rs.getInt(1);
			 }
			 
			 System.out.println("generated key: " + ps.getGeneratedKeys());
			 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return accID;

	}
	
	public void insertNewStudentInfo(Connection conn, StudentInfoBean student, int accID) {
		
		PreparedStatement ps = null;
			try {
			ps = conn.prepareStatement(INSERT_NEW_STUDENT_INFO);
		
			ps.setString(1, student.getLastName());
			ps.setString(2, student.getFirstName());
			ps.setString(3, student.getMiddleInitial());
			ps.setString(4, student.getStudentID());
			ps.setString(5, student.getYearLevel());
			ps.setString(6, student.getCourseID());
			ps.setInt(7, accID);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
}
	
	public int insertNewProfessor(Connection conn, UserAccountBean proffesor) {
		int accID = 0;
		PreparedStatement ps = null;
		String role = "prof";
		try {
			ps = conn.prepareStatement(INSERT_NEW_USER_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, proffesor.getPassword());
			ps.setString(2, proffesor.getSalt());
			ps.setString(3, role);
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			 if(rs.next()){
				 accID = rs.getInt(1);
			 }
			 
			 System.out.println("generated key: " + ps.getGeneratedKeys());
			 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return accID;

	}
	
	public void insertNewProfInfo(Connection conn, ProfessorBean professor, int accID) {
		
		PreparedStatement ps = null;
			try {
			ps = conn.prepareStatement(INSERT_NEW_PROFFESOR_INFO);
		
			ps.setString(1, professor.getProfID());
			ps.setString(2, professor.getFirstName());
			ps.setString(3, professor.getMiddleInitial());
			ps.setString(4, professor.getLastName());
			ps.setInt(5, accID);
			
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
}
	
public void insertAddress(Connection conn, StudentInfoBean student, int accID){
	PreparedStatement ps = null;
	
	try{
		ps = conn.prepareStatement(INSERT_ADDRESS);
		ps.setString(1, student.getStreet());
		ps.setString(2, student.getBarangay());
		ps.setString(3, student.getCity());
		ps.setString(4, student.getProvince());
		ps.setInt(5, accID);
		ps.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
	


public void insertStudentToSubject(Connection conn, StudentToSubjectBean subj, int counter) throws SQLException{
	PreparedStatement ps = null;
	ResultSet rs = null;
	String schoolYear = "";
	String term = "";
	rs = getCourse(conn, "BSCS-SE");
	while(rs.next()){
		schoolYear = rs.getString("school_year");
		term = rs.getString("term");
	}
	try{
		for(int ctr=0; ctr < counter; ctr++){
			ps = conn.prepareStatement(INSERT_STUDENT_TO_SUBJECT);
			System.out.println("SUBJ STUDENT IDDD: " + ctr+1 + ". " + subj.getStudentID()[ctr]);
			ps.setString(1, subj.getStudentID()[ctr]);
			ps.setString(2, subj.getSubjectID());
			ps.setString(3, subj.getCourseID());
			ps.setString(4, subj.getSection());
			ps.setString(5, schoolYear);
			ps.setString(6, term);
			ps.executeUpdate();
		}
		
	} catch (SQLException e){
		e.printStackTrace();
	}
}

private ResultSet getCourse(Connection conn, String courseID) throws SQLException{
	PreparedStatement ps = null;
	ResultSet rs = null;
	ps = conn.prepareStatement(SELECT_COURSE);
	ps.setString(1, courseID);
	rs = ps.executeQuery();
	return rs;
}

public void insertProfToSubject(Connection conn,ProfToSubjectBean ptsb) throws SQLException{
	PreparedStatement ps = null;
	ps = conn.prepareStatement(INSERT_PROF_TO_SUBJECT);
	ps.setString(1, ptsb.getProfID());
	ps.setString(2, ptsb.getSection());
	ps.setString(3, ptsb.getSubjectID());
	ps.setInt(4, ptsb.getYear());
	ps.executeUpdate();
	ps.close();
}
public void setSubjectList(Connection conn, LinkedList<AdminBean> abl) throws SQLException{
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	ps = conn.prepareStatement(SELECT_SUBJECTS);
	rs = ps.executeQuery();
	
	while(rs.next()){
		AdminBean ab = new AdminBean();
		ab.setCourse_id(rs.getString("course_id"));
		ab.setSubject_id(rs.getString("subject_id"));
		ab.setSection(rs.getString("section"));
		ab.setYear(rs.getInt("year"));
		abl.add(ab);
	}
	
	rs.close();
	ps.close();
}

public void setProfList(Connection conn, LinkedList<ProfessorBean> pbl) throws SQLException{
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	ps = conn.prepareStatement(SELECT_PROFS);
	rs = ps.executeQuery();
	
	while(rs.next()){
		ProfessorBean pb = new ProfessorBean();
		pb.setFirstName(rs.getString("first_name"));
		pb.setMiddleInitial(rs.getString("middle_initial"));
		pb.setLastName(rs.getString("last_name"));
		pb.setProfID(rs.getString("prof_id"));
		pbl.add(pb);
	}
	rs.close();
	ps.close();
}

public void setStudentList(Connection conn, LinkedList<StudentInfoBean> sib,String subject, String course, String section) throws SQLException{
	PreparedStatement ps = null;
	ResultSet rs = null;
	
	ps = conn.prepareStatement(SELECT_STUDENTS);
	//ps.setString(1, subject);
//	ps.setString(2, course);
//	ps.setString(3, section);
	
	rs = ps.executeQuery();
	
	while(rs.next()){
		if(!isStudentAlreadyEnrolled(conn, rs.getString("student_id"),section,subject,course)){
			StudentInfoBean sb = new StudentInfoBean();
			sb.setStudentID(rs.getString("student_id"));
			sb.setFirstName(rs.getString("first_name"));
			sb.setLastName(rs.getString("last_name"));
			sb.setMiddleInitial(rs.getString("middle_initial"));
			sb.setYearLevel(rs.getString("year_level"));
			sib.add(sb);
		}
	}
	
	rs.close();
	ps.close();
}
	public void insertFile(Connection conn, UploadBean uploadBean,String subjectID,String section){
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		PreparedStatement ps = null;
		
		try{
			ps = conn.prepareStatement(INSERT_FILE);
			ps.setInt(1, uploadBean.getUploader_id());
			ps.setString(2, subjectID);
			ps.setString(3, uploadBean.getMyFileFileName());
			ps.setString(4, uploadBean.getFileDestination());
			ps.setString(5, section);
			ps.setString(6, dateFormat.format(date));
			ps.executeUpdate();
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public ResultSet getUser(Connection conn, String userID) throws SQLException {
		ResultSet rs = null;
		PreparedStatement ps = null;
	
		ps = conn.prepareStatement(SELECT_USER);
		ps.setString(1, userID);

		rs = ps.executeQuery();
		return rs;
	}
	
	
	public boolean isLoginAccepted(String role, Connection conn, String userID,
			String password, UserAccountBean uab, Map<String, Object> sessionMap) throws SQLException {
		System.out.println("entered userID: " + userID);
		System.out.println("entered password: " + password);
		ResultSet rs = null;
		boolean isAccepted = false;
		String encryptedPassword = "";
		String dbPassword = "";
		String dbStudentID = "";
		int accountID = 0;
		
		PreparedStatement ps = null;
		
		if(role.equals("student")){
			ps = conn.prepareStatement(SELECT_USER_INFO_STUDENTS);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			
			while(rs.next()){
				dbStudentID = rs.getString("student_id");
				accountID = rs.getInt("account_id");
				uab.setLastName(rs.getString("last_name"));
				uab.setFirstName(rs.getString("first_name"));
				uab.setMiddleInitial(rs.getString("middle_initial"));
				uab.setYearLevel(rs.getString("year_level"));
				uab.setCourseID(rs.getString("course_id"));
				uab.setAccount_id(accountID);
				encryptedPassword = MD5.getSecurePassword(password,
						rs.getString("salt"));
				dbPassword = rs.getString("password");
				uab.setStreet(rs.getString("street"));
				uab.setBarangay(rs.getString("barangay"));
				uab.setCity(rs.getString("city"));
				uab.setProvince(rs.getString("province"));
				uab.setRole(role);
//				sessionMap.put("myUser", uab);
			}
			
			System.out.println("encrypted password from input: " + encryptedPassword);
			System.out.println("db password: " + dbPassword);
			System.out.println("studentid: " +userID);
			System.out.println("db studentid: " + dbStudentID);
			
			if(userID.equals(dbStudentID) && encryptedPassword.equals(dbPassword)){
				sessionMap.put("myUser", uab);
				isAccepted = true;
			}
		}
		else if(role.equals("prof")){
			ps = conn.prepareStatement(SELECT_USER_INFO_PROFESSOR);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			
			while(rs.next()){
				dbStudentID = rs.getString("prof_id");
				accountID = rs.getInt("account_id");
				uab.setLastName(rs.getString("last_name"));
				uab.setFirstName(rs.getString("first_name"));
				uab.setMiddleInitial(rs.getString("middle_initial"));
				uab.setAccount_id(accountID);
				uab.setRole(role);
				encryptedPassword = MD5.getSecurePassword(password,
						rs.getString("salt"));
				dbPassword = rs.getString("password");
				//sessionMap.put("myUser", uab);
			}
			
			System.out.println("encrypted password from input: " + encryptedPassword);
			System.out.println("db password: " + dbPassword);
			System.out.println("profID: " +userID);
			System.out.println("db profid: " + dbStudentID);
			
			if(userID.equals(dbStudentID) && encryptedPassword.equals(dbPassword)){
				sessionMap.put("myUser", uab);
				isAccepted = true;
			}
		} else if (role.equals("admin")){
			ps = conn.prepareStatement(SELECT_USER_INFO_PROFESSOR);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			
			while(rs.next()){
				dbStudentID = rs.getString("prof_id");
				accountID = rs.getInt("account_id");
				uab.setAccount_id(accountID);
				uab.setRole(role);
				//sessionMap.put("myUser", uab);
				encryptedPassword = MD5.getSecurePassword(password,
						rs.getString("salt"));
				dbPassword = rs.getString("password");
			}
			
			System.out.println("encrypted password from input: " + encryptedPassword);
			System.out.println("db password: " + dbPassword);
			System.out.println("profID: " +userID);
			System.out.println("db profid: " + dbStudentID);
			
			if(userID.equals(dbStudentID) && encryptedPassword.equals(dbPassword)){
				sessionMap.put("myUser", uab);
				isAccepted = true;
			}
		}
		
		System.out.println("isAccepted: " + isAccepted);
		return isAccepted;
	}
	
	public void insertMessage(Connection conn, MessageContentBean messageContentBean, int receiver, int accID) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		int messageID = -1;
		System.out.println(messageContentBean.getContent());
		ps = conn.prepareStatement(SELECT_MESSAGE_ID);
		ps.setInt(1, accID);
		ps.setInt(2, messageContentBean.getReceiver());
		rs = ps.executeQuery();
		if(rs.first() == true){
			messageID = rs.getInt("message_id");
		}
		ps.close();
		rs.close();
		if(messageID > -1){
			ps = conn.prepareStatement(INSERT_MESSAGE_CONTENT);
			ps.setInt(1, messageID);
			ps.setString(2,messageContentBean.getContent());
			ps.setInt(3, accID);
			ps.setInt(4, messageContentBean.getReceiver());
			ps.setString(5, "unread");
			ps.executeUpdate();
			ps.close();
		}else{
			System.out.println("new message id");
		ps.close();
		ps = conn.prepareStatement(INSERT_MESSAGE, Statement.RETURN_GENERATED_KEYS);
		ps.setInt(1, accID);
		ps.setInt(2, messageContentBean.getReceiver());
		ps.executeUpdate();
		rs = ps.getGeneratedKeys();
		if(rs.next()){
			messageID = rs.getInt(1);
		}
		System.out.println(messageID);
		rs.close();
		ps.close();
		ps = conn.prepareStatement(INSERT_MESSAGE_CONTENT);
		ps.setInt(1, messageID);
		ps.setString(2,messageContentBean.getContent());
		ps.setInt(3, accID);
		ps.setInt(4, messageContentBean.getReceiver());
		ps.setString(5, "unread");
		ps.executeUpdate();
		ps.close();
		}
		
	}

	public ResultSet getProfileInfo(Connection conn,String student_id) throws SQLException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		ps = conn.prepareStatement(SELECT_USER_INFO_STUDENTS);
		ps.setString(1, student_id);
		rs = ps.executeQuery();
		return rs;
	}
	
	public void setRoomList(Connection conn,String student_id,LinkedList<RoomBean> rbl,LinkedList<RoomBean> arbl,String role) throws SQLException{
		ResultSet rs = null;
		PreparedStatement ps = null;
		String schoolYear = "";
		String term = "";
		rs = getCourse(conn, "BSCS-SE");
		while(rs.next()){
			schoolYear = rs.getString("school_year");
			term = rs.getString("term");
		}
		rs.close();
		if(role.equals("student")){
			ps = conn.prepareStatement(SELECT_STUDENTSUBJECTENROLLMENT);
			ps.setString(1, student_id);
			rs = ps.executeQuery();
			while(rs.next()){
				RoomBean rb = new RoomBean();
				rb.setSubjectName(rs.getString("subject_name"));
				rb.setSubjectID(rs.getString("subject_id"));
				rb.setSubjectDay(rs.getString("subject_days"));
				rb.setSubjectRoom(rs.getString("room_number"));
				rb.setSubjectSection(rs.getString("section"));
				System.out.println(rs.getString("section"));
				rb.setSubjectTime(rs.getString("subject_time"));
				rb.setSubjectYearLevel(rs.getString("subject_year_level"));
				rb.setSchoolYear(rs.getString("school_year"));
				rb.setTerm(rs.getString("term"));
				if(rb.getSchoolYear().equals(schoolYear) && rb.getTerm().equals(term)){
					rbl.add(rb);
				}
				else{
					System.out.println("added to archive: " + rb.getSubjectName());
					arbl.add(rb);
				}
			}
			rs.close();
			ps.close();
		}
		else if(role.equals("prof")){
			rs.close();
			ps = conn.prepareStatement(SELECT_PROF_SUBJECTS);
			ps.setString(1, student_id);
			rs = ps.executeQuery();
			while(rs.next()){
				RoomBean rb = new RoomBean();
				rb.setSubjectName(rs.getString("subject_name"));
				rb.setSubjectID(rs.getString("subject_id"));
				rb.setSubjectDay(rs.getString("subject_days"));
				rb.setSubjectRoom(rs.getString("room_number"));
				rb.setSubjectSection(rs.getString("section"));
				rb.setSubjectTime(rs.getString("subject_time"));
				rb.setSubjectYearLevel(rs.getString("subject_year_level"));
				rb.setSchoolYear(rs.getString("school_year"));
				rb.setTerm(rs.getString("term"));
				if(rb.getSchoolYear().equals(schoolYear) && rb.getTerm().equals(term)){
					rbl.add(rb);
				}
				else{
					arbl.add(rb);
				}
			}
			rs.close();
			ps.close();
		}
	}
	
	public String getRole(Connection conn, String userID) throws SQLException{
		ResultSet rs = null;
		PreparedStatement ps = null;
		System.out.println("getting accountID");
		int accountID = getAccountID(conn, userID);
		System.out.println("accountID: " + accountID);
		String role = "";
		
		ps = conn.prepareStatement(SELECT_ROLE);
		ps.setInt(1, accountID);
		rs = ps.executeQuery();
		if(rs.next()){
			role = rs.getString("role");
		}
		return role;
	}
	
	private int getAccountID(Connection conn, String userID) throws SQLException{
		ResultSet rs = null;
		PreparedStatement ps = null;
		int accountID = 0;
		System.out.println("userID for accountID: " + userID);
		ps = conn.prepareStatement(SELECT_ACCOUNTID_STUDENTS);
		ps.setString(1, userID);
		rs = ps.executeQuery();
		System.out.println("fetch size: " + rs.getFetchSize());
		if(rs.next()){
			accountID = rs.getInt("account_id");
		}
		else{
			ps.close();
			rs.close();
			ps = conn.prepareStatement(SELECT_ACCOUNTID_PROF);
			ps.setString(1, userID);
			rs = ps.executeQuery();
			if(rs.next()){
				accountID = rs.getInt("account_id");
			}
		}
		
		return accountID;
	}
	
	public void setPostList(Connection conn, LinkedList<PostBean> pbl, String subjectID, String section) throws SQLException{
		ResultSet rs = null; 
	
		PreparedStatement ps = null;
		ps = conn.prepareStatement(SELECT_POST);
		ps.setString(1, subjectID);
		ps.setString(2, section);
		rs = ps.executeQuery();
		
		while(rs.next()){
			PostBean pb = new PostBean();
			LinkedList<CommentBean> commentList = new LinkedList<CommentBean>();
			
			if(rs.getString("b.first_name") != null){
				pb.setName((rs.getString("b.first_name") + " " + (rs.getString("b.last_name"))));
				pb.setContent(rs.getString("content"));
				pb.setPostID(rs.getInt("post_id"));
			}
			else{
				pb.setName((rs.getString("c.first_name") + " " + (rs.getString("c.last_name"))));
				pb.setContent(rs.getString("content"));
				pb.setPostID(rs.getInt("post_id"));
			}
			ResultSet commentRS = null;
			PreparedStatement commentPS = null;
			commentPS = conn.prepareStatement(SELECT_COMMENT);
			commentPS.setInt(1,pb.getPostID());
			commentRS = commentPS.executeQuery();
			while(commentRS.next()){
				CommentBean cb = new CommentBean();
				
				if(commentRS.getString("b.first_name") != null){
					cb.setName(commentRS.getString("b.first_name") + " " + commentRS.getString("b.last_name"));
					cb.setCommentContent(commentRS.getString("content"));
				}
				else{
					cb.setName(commentRS.getString("c.first_name") + " " + commentRS.getString("c.last_name"));
					cb.setCommentContent(commentRS.getString("content"));
				}
				commentList.add(cb);
			}
			commentRS.close();
			commentPS.close();
			pb.setCommentBeanList(commentList);
			pbl.add(pb);
		}
		rs.close();
		ps.close();
	}
	
	public void setCommentList(Connection conn, LinkedList<CommentBean> cbl, int postID) throws SQLException{
		ResultSet rs = null;
		PreparedStatement ps = null;
		ps = conn.prepareStatement(SELECT_COMMENT);
		ps.setInt(1, postID);
		rs = ps.executeQuery();
		
		while(rs.next()){
			CommentBean cb = new CommentBean();
			cb.setName(rs.getString("first_name") + rs.getString("last_name"));
			cb.setContent(rs.getString("content"));
			cbl.add(cb);
		}
		rs.close();
		ps.close();
	}
	
	public void insertPost(Connection conn, String subjectID, String section, int accountID, String content) throws SQLException{
		PreparedStatement ps = null;
		ps = conn.prepareStatement(INSERT_POST);
		ps.setString(1, content);
		ps.setInt(2, accountID);
		ps.setString(3, subjectID);
		ps.setString(4, section);
		ps.executeUpdate();
		ps.close();
	}

	public void insertComment(Connection conn, int accountID, int postID, String content) throws SQLException {
		PreparedStatement ps = null;
		ps = conn.prepareStatement(INSERT_COMMENT);
		ps.setInt(1, postID);
		ps.setString(2, content);
		ps.setInt(3, accountID);
		ps.executeUpdate();
		ps.close();
	}
	
	public ResultSet getAllSubjects(Connection conn) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ps = conn.prepareStatement(SELECT_SUBJECTS);
		rs = ps.executeQuery();
		
		return rs;
	}
	
	public void setNameList(Connection conn,ArrayList<UserAccountBean> ual,String name) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ps = conn.prepareStatement(SEARCH_NAME);
		ps.setString(1, name);
		ps.setString(2, name);
		rs = ps.executeQuery();
		
		while(rs.next()){
			UserAccountBean uab = new UserAccountBean();
			uab.setAccount_id(rs.getInt("account_id"));
			uab.setFirstName(rs.getString("first_name"));
			uab.setLastName(rs.getString("last_name"));
			System.out.println("searchBean lastNAME: " + uab.getLastName());
			ual.add(uab);
		}
		ps.close();
		rs.close();
	}

	public void setNames(Connection conn, ArrayList<SearchBean> searchNames,
			String name) throws SQLException {
		
		PreparedStatement ps= null;
		ResultSet rs = null;
		ps =conn.prepareStatement(SEARCH_NAME);
		ps.setString(1, name);
		ps.setString(2, name);
		ps.setString(3, name);
		ps.setString(4, name);
		
		rs = ps.executeQuery();
		
		while(rs.next()){
			SearchBean sb = new SearchBean();
			sb.setId(rs.getInt("account_id"));
			
			if(rs.getString("b.first_name") == null){
				sb.setName(rs.getString("c.first_name") + " " + rs.getString("c.last_name"));
			}
			else{
				sb.setName(rs.getString("b.first_name") + " " + rs.getString("b.last_name"));
				
			}
			searchNames.add(sb);
		}
		ps.close();
		ps.close();
		
	}
	
	public void setMessages(Connection conn,LinkedList<MessagesBean> mbl, int accountID) throws SQLException{
		PreparedStatement tempPs = null;
		ResultSet tempRs = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement mps = null;
		ResultSet mrs = null;
		ps = conn.prepareStatement(SELECT_MESSAGE_ID_POPULATE);
		ps.setInt(1, accountID);
	//	ps.setInt(2, accountID);
		rs = ps.executeQuery();
		while(rs.next()){
			MessagesBean mb = new MessagesBean();
			LinkedList<MessageContentBean> mcbl = new LinkedList<MessageContentBean>();
			mb.setMessageID(rs.getInt("message_id"));
			mb.setAccountID(rs.getInt("account_id"));
			mb.setReceiverID(rs.getInt("sender_id"));
			mb.setStatus(rs.getString("status"));
			if(mb.getAccountID() == accountID){
				String role = "";
				tempPs = conn.prepareStatement(SELECT_ROLE);
				tempPs.setInt(1, mb.getReceiverID());
				tempRs = tempPs.executeQuery();
				if(tempRs.next()){
					role = tempRs.getString("role");
				}
				tempRs.close();
				tempPs.close();
				if(role.equals("student")){
					tempPs = conn.prepareStatement(SELECT_USER_NAME_STUDENT);
					tempPs.setInt(1, mb.getReceiverID());
					tempRs = tempPs.executeQuery();
					while(tempRs.next()){
						mb.setHeaderName(tempRs.getString("first_name") + " " + tempRs.getString("last_name"));
					}
				}
				else if(role.equals("prof")){
					tempPs.close();
					tempRs.close();
					tempPs = conn.prepareStatement(SELECT_USER_NAME_PROF);
					tempPs.setInt(1, mb.getReceiverID());
					tempRs = tempPs.executeQuery();
					while(tempRs.next()){
						mb.setHeaderName(tempRs.getString("first_name") + " " + tempRs.getString("last_name"));
					}
				}
			}
			else{
				String role = "";
				tempPs = conn.prepareStatement(SELECT_ROLE);
				tempPs.setInt(1, mb.getAccountID());
				tempRs = tempPs.executeQuery();
				if(tempRs.next()){
					role = tempRs.getString("role");
				}
				tempRs.close();
				tempPs.close();
				if(role.equals("student")){
					tempPs = conn.prepareStatement(SELECT_USER_NAME_STUDENT);
					tempPs.setInt(1, mb.getAccountID());
					tempRs = tempPs.executeQuery();
					while(tempRs.next()){
						mb.setHeaderName(tempRs.getString("first_name") + " " + tempRs.getString("last_name"));
					}
				}
				else if(role.equals("prof")){
					tempPs.close();
					tempRs.close();
					tempPs = conn.prepareStatement(SELECT_USER_NAME_PROF);
					tempPs.setInt(1, mb.getAccountID());
					tempRs = tempPs.executeQuery();
					while(tempRs.next()){
						mb.setHeaderName(tempRs.getString("first_name") + " " + tempRs.getString("last_name"));
					}
				}
			}
			mps = conn.prepareStatement(SELECT_MESSAGE_CONTENT);
			mps.setInt(1,accountID);
			mps.setInt(2,mb.getReceiverID());
			mps.setInt(3,accountID);
			mps.setInt(4,mb.getReceiverID());
			System.out.println(mps);
			mrs = mps.executeQuery();
			if(mrs.last()){
				MessageContentBean mcb = new MessageContentBean();
				mcb.setContent(mrs.getString("content"));
				mcb.setStatus(mrs.getString("status"));
				mcbl.add(mcb);
			}
			mb.setMcbl(mcbl);
			mbl.add(mb);
		}
		rs.close();
		ps.close();
		mrs.close();
		mps.close();
	}
	
	public void setMessageContent(Connection conn,LinkedList<MessageContentBean> mcbl, int messageID, int other) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		PreparedStatement notifPS = null;
		notifPS = conn.prepareStatement(UPDATE_MESSAGE_STATUS);
		notifPS.setString(1, "read");
		notifPS.setInt(2, messageID);
		notifPS.executeUpdate();
		notifPS.close();
	
		
		ps = conn.prepareStatement(SELECT_MESSAGE_CONTENT_OPEN_MESSAGE);
		ps.setInt(1, messageID);
		ps.setInt(2, other);
		ps.setInt(3, messageID);
		ps.setInt(4, other);
		rs = ps.executeQuery();
		while(rs.next()){
			MessageContentBean mcb = new MessageContentBean();
			mcb.setContent(rs.getString("content"));
			mcb.setSender(rs.getInt("sender_id"));
			mcb.setReceiver(rs.getInt("receiver_id"));
			mcb.setStatus(rs.getString("status"));
			mcb.setMessageID((rs.getInt("message_id")));
			mcbl.add(mcb);
		}
		rs.close();
		ps.close();
	}
	
	public void setUploadList(Connection conn,LinkedList<UploadBean> ubl,String section, String subjectID) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ps = conn.prepareStatement(SELECT_UPLOAD_FILE);
		ps.setString(1, subjectID);
		ps.setString(2, section);
		rs = ps.executeQuery();
		
		while(rs.next()){
			UploadBean ub = new UploadBean();
		
			if(rs.getString("first_name") != null){
				ub.setFirstName(rs.getString("c.first_name"));
				ub.setLastName(rs.getString("c.last_name"));
			}
			else{
				ub.setFirstName(rs.getString("d.first_name"));
				ub.setLastName(rs.getString("d.last_name"));
			}
			ub.setMyFileFileName(rs.getString("file_name"));
			ub.setFileDestination(rs.getString("file"));
			ub.setMyDate(rs.getString("upload_date"));
			ub.setRole(rs.getString("role"));
			ubl.add(ub);
		}
		ps.close();
		rs.close();
	}
	
	public boolean insertSubject(Connection conn, SubjectBean sb) throws SQLException{
		PreparedStatement ps = null;
		
		
		
		try{
//			conn.setAutoCommit(false);
			ps = conn.prepareStatement(INSERT_NEW_SUBJECT);
			
			ps.setString(1, sb.getSubjectID());
			ps.setString(2, sb.getSubjectName());
			ps.setString(3, sb.getSubjectTime());
			ps.setString(4, sb.getSubjectDays());
			ps.setInt(5, sb.getYearLevel());
			ps.setString(6, sb.getSection());
			ps.setString(7, sb.getRoomNumber());
			
			ps.executeUpdate();
			
			ps = conn.prepareStatement(INSERT_NEW_CURRICULUM);
			ps.setString(1, sb.getSubjectID());
			ps.setString(2, sb.getCourseID());
			ps.setInt(3, sb.getYearLevel());
			ps.executeUpdate();
			
//			conn.commit();
			ps.close();
			return true;
		} catch(SQLException e){
			if(conn!=null){
//	            conn.rollback();
				e.printStackTrace();
			}
			return false;
		}
	}
	
	public boolean isOldPasswordCorrect(Connection conn, String oldPassword,int accountID) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		String dbPass = "";
		String salt = "";
		String oldPass = "";
		ps = conn.prepareStatement(SELECT_USERACCOUNT);
		ps.setInt(1, accountID);
		rs = ps.executeQuery();
		while(rs.next()){
			dbPass = rs.getString("password");
			salt = rs.getString("salt");
		}
		oldPass = MD5.getSecurePassword(oldPassword, salt);
		System.out.println("isOldPassCorrect: " + oldPass + " = " + dbPass);
		if(dbPass.equals(oldPass)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public int countNotification(Connection conn, int accountID) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		ps = conn.prepareStatement(COUNT_MESSAGE_NOTIFICATION);
		ps.setString(1, "unread");
		ps.setInt(2, accountID);
		rs = ps.executeQuery();
		if(rs.next()){
			return rs.getInt("Count(status)");
		}
		return 0;
	}
	
	public boolean isStudentAlreadyEnrolled(Connection conn, String studentID,String section,String subject,String course) throws SQLException{
		PreparedStatement ps = null;
		ResultSet rs = null;
		ps = conn.prepareStatement(CHECK_IF_STUDENT_IS_ALREADY_ENROLLED);
		rs = ps.executeQuery();
		while(rs.next()){
			if(studentID.equals(rs.getString("student_id")) && section.equals(rs.getString("section")) && subject.equals(rs.getString("subject_id")) && course.equals(rs.getString("course_id"))){
				return true;
			}
		}
		return false;
	}
	public void changePassword(Connection conn, UserAccountBean uab) throws SQLException, NoSuchAlgorithmException, NoSuchProviderException{
		PreparedStatement ps = null;
		ps = conn.prepareStatement(CHANGE_USER_PASSWORD);
		String salt = MD5.getSalt();
		String hashPassword = MD5.getSecurePassword(uab.getConfirmPassword(),salt);
		ps.setString(1, hashPassword);
		ps.setString(2, salt);
		ps.setInt(3, uab.getAccount_id());
		ps.executeUpdate();
		ps.close();
	}
	
	public MessageContentBean messageParams(Connection conn, int accountID, String content, String name) throws SQLException{
		MessageContentBean mb = new MessageContentBean();
		PreparedStatement ps = null;
		ResultSet rs = null;
		ps = conn.prepareStatement(GET_MESSAGE_PARAMS);
		ps.setInt(1, accountID);
		//ps.setInt(1, accountID);
		rs = ps.executeQuery();
		while(rs.next()){
			mb.setMessageID(rs.getInt("message_id"));
			System.out.println("HEYYYYYYYYYYYYYYYYYYYYYYYYYYY"+mb.getMessageID());
		}
		mb.setFullName(name);
		return mb;
	}
}
