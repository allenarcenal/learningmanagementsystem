package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.model.bean.AdminBean;
import com.lms.model.bean.ProfessorBean;
import com.lms.model.bean.StudentInfoBean;

public class AdminBusinessDelegate {
	public LinkedList<AdminBean> getSubjects(DatabaseBusinessDelegate dbg, Connection conn) throws SQLException{
		LinkedList<AdminBean> adminList = new LinkedList<AdminBean>();
		dbg.setSubjectList(conn, adminList);
		return adminList;
	}
	
	public LinkedList<StudentInfoBean> getAllStudents(DatabaseBusinessDelegate dbg, Connection conn,String subject,String course,String section) throws SQLException{
		LinkedList<StudentInfoBean> studentList = new LinkedList<StudentInfoBean>();
		dbg.setStudentList(conn, studentList,subject,course,section);
		return studentList;
	}
	
	public LinkedList<ProfessorBean> getAllProf(DatabaseBusinessDelegate dbd, Connection conn) throws SQLException{
		LinkedList<ProfessorBean> profList = new LinkedList<ProfessorBean>();
		dbd.setProfList(conn, profList);
		return profList;
	}
}
