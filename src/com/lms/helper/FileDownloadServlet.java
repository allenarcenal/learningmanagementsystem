package com.lms.helper;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import els.ss.handlers.*;

/**
 * Servlet implementation class FileDownloadServlet
 */
@WebServlet("/download.html")
public class FileDownloadServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    private static final int BUFSIZE = 4096;
    private String filePath;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		String myName = request.getParameter("myName");
		String fp = request.getParameter("filePath");
		String role = request.getParameter("role");
		System.out.println(fp);
		System.out.println("MY ACTION IS: " + action);
		if(action.equals("download")){
			response.setContentType("application/octet-stream");
			filePath = "C:/Users/Allen Arcenal/Desktop/upload/" + fp;
	        File file = new File(filePath);
	        int length   = 0;
	        ServletOutputStream outStream = response.getOutputStream();
	        ServletContext context  = getServletConfig().getServletContext();
	        String mimetype = context.getMimeType(filePath);
	        
	        // sets response content type
	        if (mimetype == null) {
	            mimetype = "application/octet-stream";
	        }
	        response.setContentType(mimetype);
	        response.setContentLength((int)file.length());
	        String fileName = (new File(filePath)).getName();
	        
	        // sets HTTP header
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
	        
	        byte[] byteBuffer = new byte[BUFSIZE];
	        DataInputStream in = new DataInputStream(new FileInputStream(file));
	        
	        // reads the file's bytes and writes them to the response stream
	        while ((in != null) && ((length = in.read(byteBuffer)) != -1))
	        {
	            outStream.write(byteBuffer,0,length);
	        }
	        
	        in.close();
	        outStream.close();
		}else{
			
			filePath = "C:/Users/Allen Arcenal/Desktop/upload/" + fp;
			if(role.equals("student")){
				CoeditController cont = new CoeditController();
				cont.setFilePath(filePath);
				cont.setStudName(myName);
				cont.setFileName(fp);
				System.out.println("FROM SERVLET" + cont.getFilePath());
				System.out.println(cont.getFileName());
				response.sendRedirect("Student.jsp");
			}else{
				CoeditController cont = new CoeditController();
				cont.setFilePath(filePath);
				cont.setStudName(myName);
				cont.setFileName(fp);
				System.out.println("FROM SERVLET" + cont.getFilePath());
				System.out.println(cont.getFileName());
				response.sendRedirect("profSheet.jsp");
			}
		}
	}

}
