package com.lms.helper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.UploadBean;
import com.lms.model.database.Database;

/**
 * Servlet implementation class CreateSheetServlet
 */
@WebServlet("/create.jsp")
public class CreateSheetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Connection connection;
	private UploadBean ub = new UploadBean();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		connection = Database.getConnection();
		//Create blank workbook
	      XSSFWorkbook workbook = new XSSFWorkbook(); 
	      //Create a blank sheet
	      XSSFSheet spreadsheet = workbook.createSheet( 
	      " Sheet ");
	      //Create row object
	      //Write the workbook in file system
	      String filename = request.getParameter("name");
	      System.out.println(filename);
	      if(StringUtils.isBlank(filename)){
	    	  System.out.println("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHEYYYYYYYYYYYYYYYYYYYYYY");
	    	  response.sendRedirect("unsuccessful.jsp");
	    	  return;
	      }
	      String destPath = "C:/Users/Allen Arcenal/Desktop/upload/";
	      FileOutputStream out = new FileOutputStream( 
	      new File(destPath + filename +".xlsx"));
	      workbook.write(out);
	      out.close();
	      
	      DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
	      int uploaderID = (int)request.getSession().getAttribute("account_id");
	      String subjectID = (String)request.getSession().getAttribute("subject_id");
	      String section = (String)request.getSession().getAttribute("section");
	      ub.setUploader_id(uploaderID);
	      ub.setFileDestination(destPath + filename +".xlsx");
	      ub.setMyFileFileName(filename +".xlsx");
	      
	      dbg.insertFile(connection, ub, subjectID, section);
	      System.out.println( 
	      "Writesheet.xlsx written successfully" );
	      response.sendRedirect("success.jsp");
	}

}
