package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.model.bean.PostBean;

public class PostBusinessDelegate {
	
	public LinkedList<PostBean> getPostList(DatabaseBusinessDelegate dbg,Connection conn,String subjectID,String section) throws SQLException{
		LinkedList<PostBean> postBeanList = new LinkedList<PostBean>();
		
		dbg.setPostList(conn, postBeanList, subjectID, section);
		
		return postBeanList;
	}
	
	public void insertPost(DatabaseBusinessDelegate dbg, Connection conn, PostBean postBean) throws SQLException{
		String content = postBean.getContent();
		String subjectID = postBean.getSubjectID();
		String section = postBean.getSection();
		int accountID = postBean.getAccountID();
		
		dbg.insertPost(conn, subjectID, section, accountID, content);
	}
	
}
