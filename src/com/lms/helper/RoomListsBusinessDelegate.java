package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.model.bean.RoomBean;

public class RoomListsBusinessDelegate {
	
	public LinkedList<RoomBean> getRooms(DatabaseBusinessDelegate dbg,Connection conn,String studentID,String role, String type) throws SQLException{
		LinkedList<RoomBean> roomList = new LinkedList<RoomBean>();
		LinkedList<RoomBean> archiveRoomList = new LinkedList<RoomBean>();
		
		dbg.setRoomList(conn, studentID, roomList,archiveRoomList,role);
		if(type.equals("current")){
		return roomList;
		}
		else{
			return archiveRoomList;
		}
	}
	
}
