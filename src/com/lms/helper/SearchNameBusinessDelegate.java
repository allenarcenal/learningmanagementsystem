package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import com.lms.model.bean.SearchBean;
import com.lms.model.bean.UserAccountBean;

public class SearchNameBusinessDelegate {
	public ArrayList<UserAccountBean> getNameList(DatabaseBusinessDelegate dbd, Connection conn, String name) throws SQLException{
		ArrayList<UserAccountBean> searchNameList = new ArrayList<UserAccountBean>();
		
		dbd.setNameList(conn, searchNameList, name);
		
		return searchNameList;
	}
	public ArrayList<SearchBean> getNames(DatabaseBusinessDelegate dbd, Connection conn, String name) throws SQLException{
		ArrayList<SearchBean> searchNames = new ArrayList<SearchBean>();
		dbd.setNames(conn,searchNames,name);
		return searchNames;
	}
}
