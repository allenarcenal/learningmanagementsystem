package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.model.bean.CommentBean;
import com.lms.model.bean.PostBean;

public class CommentBusinessDelegate {
	
	public LinkedList<CommentBean> getCommentList(DatabaseBusinessDelegate dbg, Connection conn, int postID) throws SQLException{
		LinkedList<CommentBean> commentBeanList = new LinkedList<CommentBean>();
		dbg.setCommentList(conn, commentBeanList, postID);
		return commentBeanList;
	}
	
	public void insertComment(DatabaseBusinessDelegate dbg, Connection conn, CommentBean commentBean) throws SQLException{
		String content = commentBean.getCommentContent();
		int accountID = commentBean.getAccountID();
		int postID = commentBean.getPostID();
		
		dbg.insertComment(conn, accountID, postID, content);
	}

	
}
