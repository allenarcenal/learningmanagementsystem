package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;




import com.lms.model.bean.UserAccountBean;

public class LogBusinessDelegate {
	public String getAccountRole(String userID, Connection conn, DatabaseBusinessDelegate dbd) throws SQLException{
		String role = dbd.getRole(conn, userID);
		return role;
	}
	
	public boolean loginAccount(String role, String userID, String password, UserAccountBean uab,Map<String,Object> sessionMap,Connection connection, DatabaseBusinessDelegate dbd) throws SQLException{
		return dbd.isLoginAccepted(role, connection, userID, password, uab, sessionMap);
	}
}
