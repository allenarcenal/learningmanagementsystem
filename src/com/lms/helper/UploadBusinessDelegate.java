package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.bean.lists.UploadBeanList;
import com.lms.model.bean.UploadBean;

public class UploadBusinessDelegate {
	public void setUploadList(DatabaseBusinessDelegate dbd,Connection conn,UploadBeanList ubl,String section,String subjectID) throws SQLException{
		LinkedList<UploadBean> ubList = new LinkedList<UploadBean>();
		dbd.setUploadList(conn, ubList, section, subjectID);
		ubl.setUploadBeanList(ubList);
	}
}
