package com.lms.helper;

import java.util.Map;

public class RoomBusinessDelegate {
	
	public void setRoomSession(Map<String, Object> sessionMap,String subjectID,String section){
		if(sessionMap.get("subject_id") == null){
			System.out.println("PUTTING NEW SESSIONS");
			sessionMap.put("subject_id", subjectID);
			sessionMap.put("section", section);
		}
		else{
			System.out.println("Removing then putting some");
			sessionMap.remove("subject_id");
			sessionMap.remove("section");
			sessionMap.put("subject_id", subjectID);
			sessionMap.put("section", section);
		}
	}
	
}
