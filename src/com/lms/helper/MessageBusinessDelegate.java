package com.lms.helper;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.lms.bean.lists.MessageContentBeanList;
import com.lms.bean.lists.MessagesBeanList;
import com.lms.model.bean.MessageContentBean;
import com.lms.model.bean.MessagesBean;

public class MessageBusinessDelegate {
	public void setMessages(int accountID, MessagesBeanList mbl, DatabaseBusinessDelegate dbd, Connection conn) throws SQLException{
		LinkedList<MessagesBean> messageList = new LinkedList<MessagesBean>();
		dbd.setMessages(conn, messageList, accountID);
		mbl.setMbl(messageList);
		System.out.println("done setting messages");
	}
	public void setMessageContent(int messageID,MessageContentBeanList mcbl, DatabaseBusinessDelegate dbd, Connection conn, int other) throws SQLException{
		LinkedList<MessageContentBean> messageContents = new LinkedList<MessageContentBean>();
		dbd.setMessageContent(conn, messageContents, messageID , other);
//		System.out.println(messageContents.get(0).getContent());
		mcbl.setMessageContent(messageContents);
	}
}
