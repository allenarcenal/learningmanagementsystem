package com.lms.model.bean;

import java.util.LinkedList;

public class MessagesBean extends MessageBean{
	private int messageID;
	private int receiverID;
	private int accountID;
	private String headerName;
	private LinkedList<MessageContentBean> mcbl;
	public int getReceiverID() {
		return receiverID;
	}
	public void setReceiverID(int receiverID) {
		this.receiverID = receiverID;
	}
	public LinkedList<MessageContentBean> getMcbl() {
		return mcbl;
	}
	public void setMcbl(LinkedList<MessageContentBean> mcbl) {
		this.mcbl = mcbl;
	}
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	public int getMessageID() {
		return messageID;
	}
	public void setMessageID(int messageID) {
		this.messageID = messageID;
	}
	
}
