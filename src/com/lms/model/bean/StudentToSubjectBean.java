package com.lms.model.bean;

public class StudentToSubjectBean {
	String[] studentID; 
	String subjectID;
	String section;
	String courseID;
	int year;
	
	public String[] getStudentID() {
		return studentID;
	}
	public void setStudentID(String[] studentID) {
		this.studentID = studentID;
	}
	
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public String getCourseID() {
		return courseID;
	}
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}

}
