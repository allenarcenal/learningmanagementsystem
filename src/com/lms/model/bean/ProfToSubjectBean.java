package com.lms.model.bean;

public class ProfToSubjectBean {
	String profID;
	String subjectID;
	String section;
	String courseID;
	int year;
	public String getProfID() {
		return profID;
	}
	public void setProfID(String profID) {
		this.profID = profID;
	}
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getCourseID() {
		return courseID;
	}
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
}
