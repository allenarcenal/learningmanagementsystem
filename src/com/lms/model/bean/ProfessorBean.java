package com.lms.model.bean;

public class ProfessorBean{
	private String profID;
	private String firstName;
	private String lastName;
	private String middleInitial;
	
	public String getProfID() {
		return profID;
	}
	public void setProfID(String profID) {
		this.profID = profID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
}
