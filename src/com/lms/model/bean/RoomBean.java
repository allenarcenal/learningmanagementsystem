package com.lms.model.bean;

public class RoomBean {

	private String subjectID;
	private String subjectName;
	private String subjectTime;
	private String subjectDay;
	private String subjectYearLevel;
	private String subjectSection;
	private String subjectRoom;
	private String schoolYear;
	private String term;
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getSubjectTime() {
		return subjectTime;
	}
	public void setSubjectTime(String subjectTime) {
		this.subjectTime = subjectTime;
	}
	public String getSubjectDay() {
		return subjectDay;
	}
	public void setSubjectDay(String subjectDay) {
		this.subjectDay = subjectDay;
	}
	public String getSubjectYearLevel() {
		return subjectYearLevel;
	}
	public void setSubjectYearLevel(String subjectYearLevel) {
		this.subjectYearLevel = subjectYearLevel;
	}
	public String getSubjectSection() {
		return subjectSection;
	}
	public void setSubjectSection(String subjectSection) {
		this.subjectSection = subjectSection;
	}
	public String getSubjectRoom() {
		return subjectRoom;
	}
	public void setSubjectRoom(String subjectRoom) {
		this.subjectRoom = subjectRoom;
	}
	public String getTerm() {
		return term;
	}
	public void setTerm(String term) {
		this.term = term;
	}
	public String getSchoolYear() {
		return schoolYear;
	}
	public void setSchoolYear(String schoolYear) {
		this.schoolYear = schoolYear;
	}
	
	
	
}
