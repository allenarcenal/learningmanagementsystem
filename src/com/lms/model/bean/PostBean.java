package com.lms.model.bean;

import java.util.LinkedList;

public class PostBean extends StudentInfoBean {
	private String content;
	private int accountID;
	private String subjectID;
	private String section;
	private String name;
	private int postID;
	private LinkedList<CommentBean> commentBeanList;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getAccountID() {
		return accountID;
	}
	public void setAccountID(int accountID) {
		this.accountID = accountID;
	}
	public String getSubjectID() {
		return subjectID;
	}
	public void setSubjectID(String subjectID) {
		this.subjectID = subjectID;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPostID() {
		return postID;
	}
	public LinkedList<CommentBean> getCommentBeanList() {
		return commentBeanList;
	}
	public void setCommentBeanList(LinkedList<CommentBean> commentBeanList) {
		this.commentBeanList = commentBeanList;
	}
	public void setPostID(int postID) {
		this.postID = postID;
	}
	
	
}
