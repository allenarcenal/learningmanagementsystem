package com.lms.model.bean;

import java.io.File;

public class UploadBean extends UserAccountBean{
	private int uploader_id;
	private File myFile;
	//private String myFileContentType;
	private String myFileFileName;
	private String fileDestination;
	private String myDate;
	 
	public String getMyDate() {
		return myDate;
	}
	public void setMyDate(String myDate) {
		this.myDate = myDate;
	}
	public File getMyFile() {
		return myFile;
	}
	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}
//	public String getMyFileContentType() {
//		return myFileContentType;
//	}
//	public void setMyFileContentType(String myFileContentType) {
//		this.myFileContentType = myFileContentType;
//	}
	public String getMyFileFileName() {
		return myFileFileName;
	}
	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	public int getUploader_id() {
		return uploader_id;
	}
	public void setUploader_id(int uploader_id) {
		this.uploader_id = uploader_id;
	}
	public String getFileDestination() {
		return fileDestination;
	}
	public void setFileDestination(String fileDestination) {
		this.fileDestination = fileDestination;
	}
	
}
