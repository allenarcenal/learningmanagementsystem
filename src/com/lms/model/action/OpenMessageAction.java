package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;

import com.lms.bean.lists.MessageContentBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.MessageBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class OpenMessageAction extends ActionSupport implements ParameterAware,ModelDriven<MessageContentBeanList> {
	private Map<String,String[]> params;
	private MessageContentBeanList mcbl = new MessageContentBeanList();
	private Connection connection;
	private String other;
	public String execute() throws SQLException{
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		MessageBusinessDelegate mbd = new MessageBusinessDelegate();
		int messageID = Integer.parseInt(params.get("messageID")[0]);
		setOther(params.get("other")[0]);
		
		System.out.println(params.get("other")[0]);
		int otherPerson = Integer.parseInt(params.get("qid")[0]);
		System.out.println(otherPerson);
		
		mbd.setMessageContent(messageID, mcbl, dbd, connection ,otherPerson);
		return SUCCESS;
	}
	
	@Override
	public void setParameters(Map<String, String[]> params) {
		this.params = params;
	}

	@Override
	public MessageContentBeanList getModel() {
		return mcbl;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

}
