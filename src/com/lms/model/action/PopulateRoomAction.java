package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.bean.lists.RoomBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.RoomListsBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class PopulateRoomAction extends ActionSupport implements SessionAware,ModelDriven<RoomBeanList>{

	private Map<String, Object> sessionMap;
	private RoomBeanList roomBeanList = new RoomBeanList();
	private Connection connection;
	
	public String execute() throws SQLException{
		System.out.println("populate room called");
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		RoomListsBusinessDelegate rlbg = new RoomListsBusinessDelegate();
		connection = Database.getConnection();
		String role = (String)sessionMap.get("role");
		String userID = (String)sessionMap.get("user_id");	
		roomBeanList.setRoomBeanList(rlbg.getRooms(dbg, connection, userID,role,"current"));
		roomBeanList.setArchiveRoomBeanList(rlbg.getRooms(dbg, connection, userID,role,"archive"));
		System.out.println("populate room done");
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public RoomBeanList getModel() {
		// TODO Auto-generated method stub
		return roomBeanList;
	}

}
