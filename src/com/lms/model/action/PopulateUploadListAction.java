package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.bean.lists.UploadBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.UploadBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class PopulateUploadListAction extends ActionSupport implements
		SessionAware, ModelDriven<UploadBeanList> {
	
	private Map<String,Object> sessionMap;
	private UploadBeanList uploadBeanList = new UploadBeanList();
	private Connection connection;
	
	public String execute() throws SQLException{
		DatabaseBusinessDelegate dbd =  new DatabaseBusinessDelegate();
		UploadBusinessDelegate ubd = new UploadBusinessDelegate();
		String section = sessionMap.get("section").toString();
		String subjectID = sessionMap.get("subject_id").toString();
		connection = Database.getConnection();
		
		ubd.setUploadList(dbd, connection, uploadBeanList, section, subjectID);
		
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public UploadBeanList getModel() {
		// TODO Auto-generated method stub
		return uploadBeanList;
	}

}
