package com.lms.model.action;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class ChangePasswordAction extends ActionSupport implements
		SessionAware, ModelDriven<UserAccountBean> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection connection;
	private Map<String,Object> sessionMap;
	private UserAccountBean user = new UserAccountBean();
	
	public String execute() throws SQLException, NoSuchAlgorithmException, NoSuchProviderException{
		System.out.println("CHANGE PASS CALLED");
		System.out.println("old password: " + user.getOldPassword());
		System.out.println("new password: " + user.getPassword());
		System.out.println("confirm password: " + user.getConfirmPassword());
		
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		int accountID = (int)sessionMap.get("account_id");
		user.setAccount_id(accountID);
		if(dbd.isOldPasswordCorrect(connection, user.getOldPassword(), accountID) == false){
			addFieldError("error", "invalid password");
			return ERROR;
		}
		if(user.getPassword().equals(user.getConfirmPassword()) == false){
			addFieldError("error", "password not match");
			return ERROR;
		}
		dbd.changePassword(connection, user);
		return SUCCESS;
	}
	
	@Override
	public void validate(){
		
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public UserAccountBean getModel() {
		return user;
	}

}
