package com.lms.model.action;

import java.sql.SQLException;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.SubjectBean;
import com.lms.model.database.Database;
import java.sql.Connection;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class CreateSubjectAction extends ActionSupport implements ModelDriven<SubjectBean>{
	private Connection connection;
	private SubjectBean sb = new SubjectBean();
	
	public String execute() throws SQLException{
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		
		System.out.println("RON TUKMOL CALLED:" + sb.getSubjectDays());
		
		boolean isOkay = dbg.insertSubject(connection, sb);
		if(isOkay == false){
			return ERROR;
		}
		return SUCCESS;
	}
	
	@Override
	public void validate(){

	}

	@Override
	public SubjectBean getModel() {
		return sb;
	}

}
