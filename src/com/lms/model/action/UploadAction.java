package com.lms.model.action;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.Property;
import com.lms.model.bean.UploadBean;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class UploadAction extends ActionSupport implements SessionAware, ModelDriven<UploadBean>{
	private String destPath;
	private Map<String, Object> sessionMap;
	private Connection connection;
	private UploadBean ub = new UploadBean();

	public String execute() throws IOException {
		System.out.println("upload action called");
		Property prop = new Property();
		if(StringUtils.isBlank(ub.getMyFileFileName())){
			return ERROR;
		}
		/* Copy file to a safe location */
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		int uploaderID = (int)sessionMap.get("account_id");
		String subjectID = (String)sessionMap.get("subject_id");
		String section = (String)sessionMap.get("section");
		UserAccountBean uab = (UserAccountBean) sessionMap.get("myUser");
		destPath = prop.getPropValue("fileDestination");

		try {
			System.out.println("Src File name: " + ub.getMyFile());
			System.out.println("Dst File name: " + ub.getMyFileFileName());
			
			String myExt[] = ub.getMyFileFileName().split("\\.");
			
			System.out.println(myExt[myExt.length-1]);
			System.out.println(uab.getRole());
			
			File destFile = new File(destPath, ub.getMyFileFileName());
			FileUtils.copyFile(ub.getMyFile(), destFile);
			
			ub.setUploader_id(uploaderID);
			ub.setFileDestination(destPath + "/" + ub.getMyFileFileName());
			
			System.out.println("Student id in upload: " + uploaderID);
			System.out.println("Destination of upload: " + (destPath + "/" + ub.getMyFileFileName()));
			
			dbg.insertFile(connection, ub, subjectID,section);
			
		} catch (IOException e) {
			e.printStackTrace();
			return ERROR;
		}

		return SUCCESS;
	}

	@Override
	public UploadBean getModel() {
		return ub;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}