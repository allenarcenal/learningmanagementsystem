package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.AdminBean;
import com.lms.model.bean.StudentToSubjectBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class InsertStudentToSubjectAction extends ActionSupport implements ModelDriven<StudentToSubjectBean>,SessionAware{
	private Connection connection;
	private StudentToSubjectBean stsb = new StudentToSubjectBean();
	private Map<String, Object> sessionMap;
	
	public String execute() throws SQLException{
		System.out.println("INSERTSTUDENTCALLED");
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		
		int studentsCount = stsb.getStudentID().length;
		System.out.println("STUDENT COUNT: " + studentsCount);
		System.out.println("section: " + stsb.getSection());
		dbg.insertStudentToSubject(connection, stsb, studentsCount);
		System.out.println("STUDENTS INSERTED");
		sessionMap.remove("stat");
		sessionMap.remove("adminBean");
		return SUCCESS;
	}

	@Override
	public StudentToSubjectBean getModel() {
		return stsb;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;	
	}

}
