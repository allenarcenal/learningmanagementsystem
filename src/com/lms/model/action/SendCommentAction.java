package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.CommentBusinessDelegate;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.CommentBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class SendCommentAction extends ActionSupport implements SessionAware,ModelDriven<CommentBean>,ParameterAware {
	private Map<String,Object> sessionMap;
	private Map<String,String[]> params;
	private CommentBean commentBean = new CommentBean();
	private Connection connection;
	
	public String execute() throws SQLException{
		System.out.println("sendcomment called");
		CommentBusinessDelegate cbd = new CommentBusinessDelegate();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		connection = Database.getConnection();
		
		int accountID = (int)sessionMap.get("account_id");
		String content = commentBean.getCommentContent();
		int postID = commentBean.getPostID();
		System.out.println(commentBean.getCommentContent());
		System.out.println(postID);
		commentBean.setAccountID(accountID);
		commentBean.setCommentContent(content);
		commentBean.setPostID(postID);
		
		cbd.insertComment(dbd, connection, commentBean);
		
		System.out.println("sendcommentAct done");
		return SUCCESS;
	}
	
	@Override
	public CommentBean getModel() {
		return commentBean;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public void setParameters(Map<String, String[]> params) {
		this.params = params;
	}

}
