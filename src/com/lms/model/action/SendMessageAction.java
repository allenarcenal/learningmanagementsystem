package com.lms.model.action;

import com.lms.model.bean.UserAccountBean;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class SendMessageAction extends ActionSupport implements ModelDriven<UserAccountBean>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int accountID[];
	private String msgType;
	public String execute(){
		
		int receiver = getAccountID()[0];
		String msgType = getMsgType();
		System.out.println("receiver: " + receiver);
		System.out.println("msgtype: " + msgType);
		return SUCCESS;
	}

	@Override
	public UserAccountBean getModel() {
		return null;
	}

	public int[] getAccountID() {
		return accountID;
	}

	public void setAccountID(int id[]) {
		this.accountID = id;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
