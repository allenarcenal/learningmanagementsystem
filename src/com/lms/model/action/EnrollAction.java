package com.lms.model.action;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.model.bean.AdminBean;
import com.opensymphony.xwork2.ActionSupport;

public class EnrollAction extends ActionSupport implements SessionAware{
	
	private Map<String, Object> sessionMap;
	private AdminBean adminBean = new AdminBean();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String execute(){
		System.out.println("enroll action called");
		System.out.println(getAdminBean().getSubject_id());
		String stat = sessionMap.get("stat").toString();
		sessionMap.put("adminBean", getAdminBean());
		System.out.println("stat: " + stat);
		if(stat.equals("student")){
			return "student";
		}
		else{
			return "prof";
		}
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public AdminBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(AdminBean adminBean) {
		this.adminBean = adminBean;
	}

}
