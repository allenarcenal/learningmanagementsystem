package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class NotificationAction extends ActionSupport implements SessionAware {
	private int notification;
	private Map<String,Object> sessionMap;
	private Connection conn;
	
	public String execute() throws SQLException{
		conn = Database.getConnection();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		int accountID = (int) sessionMap.get("account_id");
		setNotification(dbd.countNotification(conn, accountID));
		System.out.println("message notif: " + getNotification());
		return SUCCESS;
	}

	public int getNotification() {
		return notification;
	}

	public void setNotification(int notification) {
		this.notification = notification;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
}
