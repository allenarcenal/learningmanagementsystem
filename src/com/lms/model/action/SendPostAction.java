package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.PostBusinessDelegate;
import com.lms.model.bean.PostBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class SendPostAction extends ActionSupport  implements SessionAware,ModelDriven<PostBean>{
	private Map<String, Object> sessionMap;
	private PostBean postBean = new PostBean();
	private Connection connection;
	
	public String execute() throws SQLException{
		PostBusinessDelegate pbd = new PostBusinessDelegate();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		connection = Database.getConnection();
		
		
		String content = postBean.getContent();
		String subjectID = sessionMap.get("subject_id").toString();
		String section = sessionMap.get("section").toString();
		int accountID = (int)sessionMap.get("account_id");
		
		postBean.setAccountID(accountID);
		postBean.setContent(content);
		postBean.setSubjectID(subjectID);
		postBean.setSection(section);
		
		pbd.insertPost(dbd, connection, postBean);
		
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public PostBean getModel() {
		return postBean;
	}

}
