package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.bean.lists.StudentBeanList;
import com.lms.helper.AdminBusinessDelegate;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.AdminBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class PopulateStudentAction extends ActionSupport implements ModelDriven<StudentBeanList>,SessionAware{
	private Connection connection;
	private Map<String, Object> sessionMap;
	private StudentBeanList sbl = new StudentBeanList();
	private AdminBean adminBean;
	public String execute() throws SQLException{
		System.out.println("INSERT STUDENT ACTION CALLED");
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		AdminBusinessDelegate abd = new AdminBusinessDelegate();
		setAdminBean((AdminBean)sessionMap.get("adminBean"));
		sbl.setStudentListBean(abd.getAllStudents(dbg, connection,getAdminBean().getSubject_id(),getAdminBean().getCourse_id(),getAdminBean().getSection()));
		return SUCCESS;
	}
	
	@Override
	public StudentBeanList getModel() {
		return sbl;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public AdminBean getAdminBean() {
		return adminBean;
	}

	public void setAdminBean(AdminBean adminBean) {
		this.adminBean = adminBean;
	}


}
