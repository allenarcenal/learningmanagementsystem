package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.MessageBean;
import com.lms.model.bean.MessageContentBean;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class MessageAction extends ActionSupport implements SessionAware, ModelDriven<MessageContentBean>{
	private Map<String, Object> sessionMap;
	private Connection connection;
	private MessageContentBean mcb = new MessageContentBean();
	private String msgType;
	private String addParam;
	private String recName;
	private String receiver;
	
	public String execute() throws SQLException{
		System.out.println("RECEIVER IS === " + getReceiver());
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		int accID = (int)sessionMap.get("account_id");
		String content = mcb.getContent();
		int magicNum = mcb.getReceiver();
		System.out.println(magicNum);
		String msgType = getMsgType();
		System.out.println(msgType);
		System.out.println(content);
		UserAccountBean uab = (UserAccountBean)sessionMap.get("myUser");
		System.out.println("HHHHHHHHHHHHHHH" + uab.getRole());
		
		try {
			dbg.insertMessage(connection, mcb, magicNum, accID);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(msgType.equals("reply")){
			System.out.println("RECEIVER IS === " + receiver);
			mcb = dbg.messageParams(connection, accID,content, recName);
//			setAddParam("?messageID="+ mcb.getMessageID() +"&other=" + mcb.getFirstName() + " " + mcb.getLastName());
			setAddParam("?messageID="+ accID +"&other=" + getRecName()+"&qid=" + magicNum);
			return "replied";
		}else{
			return SUCCESS;
		}
	}
	
	@Override
	public void validate(){
	}
	

	@Override
	public MessageContentBean getModel() {
		return mcb;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getAddParam() {
		return addParam;
	}

	public void setAddParam(String addParam) {
		this.addParam = addParam;
	}

	public String getRecName() {
		return recName;
	}

	public void setRecName(String recName) {
		this.recName = recName;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

}
