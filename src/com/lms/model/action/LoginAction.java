package com.lms.model.action;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import nl.captcha.Captcha;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.LogBusinessDelegate;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<UserAccountBean>{
	private static String STUDENT = "student";
	private static String PROFESSOR = "prof";
	private static String ADMIN = "admin";
	private Map<String, Object> sessionMap;
	private Connection connection;
	private Captcha captcha;
	private UserAccountBean uab = new UserAccountBean();
	private String userID;
	private boolean isLoginAccepted = false;
	
	DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
	LogBusinessDelegate lbd = new LogBusinessDelegate();
	
	public String execute() throws SQLException{
		connection = Database.getConnection();
		System.out.println("userid: " + getUserID());
		System.out.println("password: " + uab.getPassword());
		
		System.out.println("captcha: " + getCaptcha());
		String role = lbd.getAccountRole(getUserID(), connection, dbg);
		sessionMap.put("role", role);
		System.out.println("role: " + role);
		
		if(role.equals(STUDENT)){
			isLoginAccepted = lbd.loginAccount(role, getUserID(), uab.getPassword(), uab, sessionMap, connection, dbg);
			if(isLoginAccepted == false){
				addFieldError("error", "Invalid User ID/Password");
				return ERROR;
			}
			sessionMap.put("user_id",getUserID());
			sessionMap.put("account_id", uab.getAccount_id());
			System.out.println("Logged STUDENT ID: " + sessionMap.get("user_id"));
			System.out.println("Logged account ID: " + uab.getAccount_id());
			
			return "student";
		}
		else if(role.equals(PROFESSOR)){
			isLoginAccepted = lbd.loginAccount(role, getUserID(), uab.getPassword(), uab, sessionMap, connection, dbg);
			if(isLoginAccepted == false){
				addFieldError("error", "Invalid User ID/Password");
				return ERROR;
			}
			sessionMap.put("user_id",getUserID());
			sessionMap.put("account_id", uab.getAccount_id());
			System.out.println("Logged PROF ID: " + sessionMap.get("user_id"));
			System.out.println("Logged account ID: " + uab.getAccount_id());
			
			return "prof";
			
		} else if (role.equals(ADMIN)){
			isLoginAccepted = lbd.loginAccount(role, getUserID(), uab.getPassword(), uab, sessionMap, connection, dbg);
			if(isLoginAccepted == false){
				addFieldError("error", "Invalid User ID/Password");
				return ERROR;
			}
			sessionMap.put("user_id", getUserID());
			sessionMap.put("account_id", uab.getAccount_id());
			System.out.println("Logged ADMIN ID: " + sessionMap.get("user_id"));
			System.out.println("Logged account ID: " + uab.getAccount_id());
			
			return "admin";
		}
		else{
			addFieldError("error", "Invalid User ID/Password");
			return ERROR;
		}
		
		
		
	}
	
	@Override
	public void validate(){
		setCaptcha((Captcha)sessionMap.get(Captcha.NAME));
		if(!getCaptcha().isCorrect(uab.getAnswer())){
			addFieldError("error", "Invalid Captcha Validation");
		}
		
		
		
	}
	
	
	@Override
	public UserAccountBean getModel() {
		return uab;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;	
	}
	
	public Captcha getCaptcha() {
		return captcha;
	}

	public void setCaptcha(Captcha captcha) {
		this.captcha = captcha;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	
}
