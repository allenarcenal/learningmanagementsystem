package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.bean.lists.MessagesBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.MessageBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class PopulateMessageListAction extends ActionSupport implements
		SessionAware,ModelDriven<MessagesBeanList> {
	
	private Map<String,Object> sessionMap;
	private Connection connection;
	private MessagesBeanList mbl = new MessagesBeanList();
	
	public String execute() throws SQLException{
		connection = Database.getConnection();
		int accountID = (int) sessionMap.get("account_id");
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		MessageBusinessDelegate mbd = new MessageBusinessDelegate();
		
		mbd.setMessages(accountID, mbl, dbd, connection);
		System.out.println("message list populated");
		return SUCCESS;
	}
	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

	@Override
	public MessagesBeanList getModel() {
		return mbl;
	}

}
