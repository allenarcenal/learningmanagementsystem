package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.ProfToSubjectBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class InsertProfToSubjects extends ActionSupport implements ModelDriven<ProfToSubjectBean>,SessionAware{
	private Connection connection;
	private ProfToSubjectBean ptsb = new ProfToSubjectBean();
	private Map<String, Object> sessionMap;
	
	public String execute() throws SQLException{
		System.out.println("profinsertcalled");
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		
		dbd.insertProfToSubject(connection, ptsb);
		sessionMap.remove("stat");
		sessionMap.remove("adminBean");
		return SUCCESS;
	}
	
	@Override
	public ProfToSubjectBean getModel() {
		return ptsb;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
