package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.bean.lists.PostBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.PostBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class PopulatePostAction extends ActionSupport implements SessionAware,ModelDriven<PostBeanList>{
	
	private PostBeanList postBeanList = new PostBeanList();
	private Map<String, Object> sessionMap;
	private Connection connection;
	
	public String execute() throws SQLException{
		System.out.println("populatepostAct called");
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		PostBusinessDelegate ppbd = new PostBusinessDelegate();
		connection = Database.getConnection();
		String subjectID = sessionMap.get("subject_id").toString();
		String section = sessionMap.get("section").toString();
		System.out.println(subjectID + "HEYOOO" + section);
		postBeanList.setPostListBean(ppbd.getPostList(dbg, connection, subjectID, section));
		//sessionMap.put("post", postBeanList);
		
		
		System.out.println("populatepostAct done");
		return SUCCESS;
	}
	
	@Override
	public PostBeanList getModel() {
		// TODO Auto-generated method stub
		return postBeanList;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
