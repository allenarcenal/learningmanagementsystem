package com.lms.model.action;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import nl.captcha.Captcha;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.exceptions.PasswordNotMatchException;
import com.lms.exceptions.StudentIDTakenException;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.StudentInfoBean;
import com.lms.model.bean.UserAccountBean;
import com.lms.model.database.Database;
import com.lms.utils.MD5;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class RegisterAction extends ActionSupport implements SessionAware, ModelDriven<UserAccountBean>{
	private Map<String, Object> sessionMap;
	private Connection connection;
	private UserAccountBean uab = new UserAccountBean();
	
	public String execute() throws SQLException{
		return SUCCESS;
	}
	
	@Override
	public void validate(){
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		Captcha captcha = (Captcha) sessionMap.get(Captcha.NAME);
		if(captcha.isCorrect(uab.getAnswer())){
			try {
				dbg.validateRegister(connection, uab);
				String salt= "";
				
				try{
					salt = MD5.getSalt();
				} catch(NoSuchAlgorithmException | NoSuchProviderException e){
					
				}
				uab.setSalt(salt);
				String encryptedPass = MD5.getSecurePassword(uab.getPassword(), uab.getSalt());
				//System.out.println("ENCRYPTEDPASS" + encryptedPass);
				uab.setPassword(encryptedPass);
				int accID = dbg.insertNewStudent(connection, uab);
				dbg.insertNewStudentInfo(connection, uab, accID);
				dbg.insertAddress(connection, uab, accID);
				
			} catch (PasswordNotMatchException pnme) {
				addFieldError("error", pnme.getMessage());
			} catch (StudentIDTakenException sidte) {
				addFieldError("error", sidte.getMessage());
			}
		} else {
			addFieldError("error", "Invalid Captcha Validation");
		}
	}
	
	public UserAccountBean getSib() {
		return uab;
	}

	
	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
	
	@Override
	public UserAccountBean getModel() {
		return uab;
	}


}
