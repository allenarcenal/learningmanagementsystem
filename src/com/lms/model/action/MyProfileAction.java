package com.lms.model.action;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.StudentInfoBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class MyProfileAction extends ActionSupport implements ModelDriven<StudentInfoBean>,SessionAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Connection connection;
	private StudentInfoBean sib = new StudentInfoBean();
	private Map<String, Object> sessionMap;
	
	
	
	public String execute() throws SQLException{
		
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		String studentId = (String)sessionMap.get("student_id");
		//System.out.println("profile sid: " + studentId);
		ResultSet rs = dbg.getProfileInfo(connection,studentId);
		
		while(rs.next()){
			sib.setFirstName(rs.getString("first_name"));
			sib.setMiddleInitial(rs.getString("middle_initial"));
			sib.setLastName(rs.getString("last_name"));
			sib.setYearLevel(rs.getString("year_level"));
			sib.setCourseID(rs.getString("course_id"));
			sib.setStreet(rs.getString("street"));
			sib.setBarangay(rs.getString("barangay"));
			sib.setCity(rs.getString("city"));
			sib.setProvince(rs.getString("province"));
		}
		
		return SUCCESS;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}
	@Override
	public StudentInfoBean getModel() {
		// TODO Auto-generated method stub
		return sib;
	}
}
