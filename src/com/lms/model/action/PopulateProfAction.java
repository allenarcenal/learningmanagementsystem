package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;

import com.lms.bean.lists.ProfBeanList;
import com.lms.helper.AdminBusinessDelegate;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.AdminBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public class PopulateProfAction extends ActionSupport implements ModelDriven<ProfBeanList>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Connection connection;
	private ProfBeanList pbl = new ProfBeanList();
	
	public String execute() throws SQLException{
		System.out.println("populate prof called");
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		AdminBusinessDelegate abd = new AdminBusinessDelegate();
		pbl.setProfList(abd.getAllProf(dbd, connection));
		return SUCCESS;
	}
	
	@Override
	public ProfBeanList getModel() {
		return pbl;
	}
	
}
