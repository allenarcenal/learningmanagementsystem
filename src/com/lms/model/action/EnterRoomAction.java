package com.lms.model.action;

import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.SessionAware;

import com.lms.helper.RoomBusinessDelegate;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
public class EnterRoomAction extends ActionSupport implements SessionAware,ParameterAware{

	
	
	private Map<String, String[]> parameters;
	private Map<String, Object> sessionMap;

	public String execute(){
		RoomBusinessDelegate rbd = new RoomBusinessDelegate();
		String subjectID = parameters.get("subject")[0];
		String subjectSection = parameters.get("section")[0];
		
		rbd.setRoomSession(sessionMap, subjectID,subjectSection);
		System.out.println("you are in: " + subjectID);
		System.out.println("you are in: " + subjectSection);
		
		return SUCCESS;
	}
	
	@Override
	public void setParameters(Map<String, String[]> parameters) {
		this.parameters = parameters;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
		this.sessionMap = sessionMap;
	}

}
