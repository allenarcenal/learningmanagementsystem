package com.lms.model.action;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.lms.bean.lists.SearchBeanList;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.helper.SearchNameBusinessDelegate;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

@SuppressWarnings("serial")
public class SearchNameAction extends ActionSupport implements ModelDriven<SearchBeanList>,Serializable {
	private SearchBeanList searchBeanList = new SearchBeanList();
	private Connection connection;
	private String term;
	private String q;
	
	public String execute() throws SQLException{
		System.out.println("search action called");
		System.out.println(getQ());
		DatabaseBusinessDelegate dbd = new DatabaseBusinessDelegate();
		SearchNameBusinessDelegate snbd = new SearchNameBusinessDelegate();
		connection = Database.getConnection();
		
		//searchBeanList.setSearchBeanList(snbd.getNameList(dbd, connection, q));
		searchBeanList.setSearchList(snbd.getNames(dbd, connection, q));
		System.out.println(searchBeanList.getSearchList().get(0).getName());
		return SUCCESS;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	@Override
	public SearchBeanList getModel() {
		return searchBeanList;
	}

	public String getQ() {
		return q;
	}

	public void setQ(String q) {
		this.q = q;
	}
}
