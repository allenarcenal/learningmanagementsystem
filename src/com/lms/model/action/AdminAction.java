package com.lms.model.action;

import java.sql.Connection;
import java.sql.SQLException;

import com.lms.bean.lists.AdminBeanList;
import com.lms.helper.AdminBusinessDelegate;
import com.lms.helper.DatabaseBusinessDelegate;
import com.lms.model.bean.AdminBean;
import com.lms.model.database.Database;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.ParameterAware;
import org.apache.struts2.interceptor.SessionAware;


@SuppressWarnings("serial")
public class AdminAction extends ActionSupport implements ModelDriven<AdminBeanList>,ParameterAware,SessionAware{
	private Connection connection;
	private AdminBeanList abl = new AdminBeanList();
	private Map<String, String[]> params;
	private Map<String, Object> sessionMap;
	
	public String execute() throws SQLException{
		System.out.println("ADMIN ACTION CALLED");
		String stat = params.get("stat")[0];
		connection = Database.getConnection();
		DatabaseBusinessDelegate dbg = new DatabaseBusinessDelegate();
		AdminBusinessDelegate abd = new AdminBusinessDelegate();
		sessionMap.put("stat", stat);
		abl.setAdminListBean(abd.getSubjects(dbg, connection));
		
		
		return SUCCESS;
	}
	
	@Override
	public AdminBeanList getModel() {
		return abl;
	}

	@Override
	public void setParameters(Map<String, String[]> params) {
		this.params = params;
	}

	@Override
	public void setSession(Map<String, Object> sessionMap) {
	this.sessionMap = sessionMap;	
	}

}
