package com.lms.model.database;

public interface SQL {
	String DB_SOURCE = "java:comp/env/jdbc/thesis";
	
	String INSERT_NEW_STUDENT_INFO = "INSERT into students(last_name, first_name, middle_initial, student_id, year_level, course_id,account_id)"
			+"VALUES (?,?,?,?,?,?,?)";
	
	String INSERT_STUDENT_TO_SUBJECT = "INSERT into student_subject_enrollment(student_id, subject_id, course_id, section,school_year,term) VALUES(?,?,?,?,?,?)";

	String INSERT_PROF_TO_SUBJECT = "UPDATE subjects SET prof_id = ? WHERE section = ? AND subject_id = ? AND subject_year_level = ?";
	
	String INSERT_NEW_USER_ACCOUNT = "INSERT into user_accounts(password, salt, role) VALUES(?,?,?)";
	
	//String INSERT_NEW_STUDENT_ACCOUNT = "INSERT into user_accounts(password, salt) VALUES(?,?)";

	String INSERT_NEW_PROFFESOR_INFO = "INSERT into professor(prof_id, first_name, middle_initial, last_name, account_id)"
			+"VALUES (?,?,?,?,?)";
	
	String INSERT_ADDRESS = "INSERT into address(street, barangay, city, province, account_id) VALUES(?,?,?,?,?)";

	String INSERT_FILE = "INSERT into file_upload(account_id, subject_id,file_name, file, section,upload_date) VALUES (?,?,?,?,?,?)";
	
	String SELECT_USER = "SELECT * FROM user_accounts a INNER JOIN students b ON a.account_id = b.account_id "
			+ 				"INNER JOIN professor c ON a.account_id = c.account_id where student_id = ?";
	
	String SELECT_ROLE = "SELECT role FROM user_accounts WHERE account_id = ?";
	
	String SELECT_ACCOUNTID_STUDENTS = "SELECT account_id FROM students where student_id = ?";
	
	String INSERT_MESSAGE = "INSERT into messaging(account_id, receiver_id) VALUES(?,?)";
	
	String INSERT_MESSAGE_CONTENT = "INSERT INTO message_content(message_id,content,sender_id,receiver_id,status) VALUES(?,?,?,?,?)";
	
	String SELECT_ACCOUNTID_PROF = "SELECT account_id FROM professor where prof_id = ?";
	
	String SELECT_USER_INFO_STUDENTS = "SELECT * FROM students a INNER JOIN address b ON a.account_id = b.account_id "
			+ "INNER JOIN user_accounts c ON a.account_id = c.account_id "
			+ "WHERE a.student_id = ?";
	
	String SELECT_USER_INFO_PROFESSOR = "SELECT * FROM professor a INNER JOIN user_accounts b ON a.account_id = b.account_id "
			+ "WHERE a.prof_id = ?";
	
	String SELECT_USERACCOUNT = "SELECT * FROM user_accounts where account_id = ?";
	
	String SELECT_ADDRESS = "SELECT * FROM address where account_id = ?";
	
	String SELECT_STUDENTSUBJECTENROLLMENT = "SELECT * FROM student_subject_enrollment a INNER JOIN subjects b ON a.subject_id = b.subject_id where student_id = ?";

	String SELECT_POST = "SELECT * FROM post a LEFT OUTER JOIN students b ON a.account_id = b.account_id LEFT OUTER JOIN professor c ON a.account_id = c.account_id WHERE subject_id = ? AND section = ? ORDER by a.post_date desc";

	String INSERT_POST = "INSERT INTO post(content,account_id,subject_id,section) VALUES (?,?,?,?)";
	
	String SELECT_COMMENT = "SELECT * FROM comment a LEFT OUTER JOIN students b ON a.account_id = b.account_id LEFT OUTER JOIN professor c ON a.account_id = c.account_id WHERE post_id = ? ORDER BY a.comment_date asc";
	
	String SELECT_USER_NAME_STUDENT = "SELECT first_name,last_name FROM user_accounts a INNER JOIN students b ON a.account_id = b.account_ID WHERE a.account_id = ?";
	
	String SELECT_USER_NAME_PROF = "SELECT first_name,last_name FROM user_accounts a INNER JOIN professor b ON a.account_id = b.account_ID WHERE a.account_id = ?";
	
	String INSERT_COMMENT = "INSERT INTO comment(post_id,content,account_id) VALUES(?,?,?)";
	
	String SELECT_SUBJECTS = "SELECT * FROM curriculums a INNER JOIN subjects b ON a.subject_id = b.subject_id";
	
	String SELECT_PROF_SUBJECTS = "SELECT DISTINCT * FROM subjects a INNER JOIN student_subject_enrollment b ON a.subject_id = b.subject_id WHERE prof_id = ? GROUP BY a.subject_id";
	
	String SELECT_STUDENTS = "SELECT * FROM students";
	
	String SELECT_PROFS = "SELECT * FROM professor";
	
	String SELECT_COURSE = "SELECT * FROM course WHERE course_id = ?";
	
	String SEARCH_NAME = "SELECT * FROM user_accounts a LEFT OUTER JOIN students b ON a.account_id = b.account_id LEFT OUTER JOIN professor c ON a.account_id = c.account_id WHERE b.first_name LIKE CONCAT(?, '%') "
			+ "OR b.last_name LIKE CONCAT(?,'%') OR  c.first_name LIKE CONCAT(?, '%') OR  c.last_name LIKE CONCAT(?, '%')";
	
	String SELECT_MESSAGE_ID = "SELECT * FROM messaging WHERE account_id = ? AND receiver_id = ?";
	
//	String SELECT_MESSAGE_ID_POPULATE = "SELECT * FROM messaging WHERE account_id = ? OR receiver_id = ?";
	String SELECT_MESSAGE_ID_POPULATE = "SELECT DISTINCT message_content.message_id, messaging.account_id,message_content.receiver_id,message_content.sender_id, message_content.status FROM messaging INNER JOIN message_content ON messaging.message_id=message_content.message_id WHERE messaging.receiver_id = ?";	
//	String SELECT_MESSAGE_ID_POPULATE = "SELECT DISTINCT * FROM messaging a INNER JOIN message_content b ON a.message_id = b.message_id WHERE a.account_id = ? OR a.receiver_id = ?";
	
//	String SELECT_MESSAGE_CONTENT = "SELECT * FROM message_content where message_id = ?";
//	String SELECT_MESSAGE_CONTENT = "SELECT * FROM message_content INNER JOIN messaging ON message_content.message_id=messaging.message_id where message_content.message_id = ? ORDER BY content_time DESC LIMIT 1 ";
//	String SELECT_MESSAGE_CONTENT = "SELECT * FROM message_content where message_id = ?";
	String SELECT_MESSAGE_CONTENT = "SELECT * FROM message_content INNER JOIN messaging ON message_content.message_id=messaging.message_id where (message_content.sender_id = ? AND message_content.receiver_id = ?) OR (message_content.receiver_id = ? AND message_content.sender_id= ?) ORDER BY content_time DESC LIMIT 1 ";
	String SELECT_MESSAGE_CONTENT_OPEN_MESSAGE = "SELECT * FROM message_content INNER JOIN messaging ON message_content.message_id=messaging.message_id where (message_content.sender_id = ? AND message_content.receiver_id = ?) OR (message_content.receiver_id = ? AND message_content.sender_id= ?) ORDER BY message_content.content_time";
	
	String SELECT_UPLOAD_FILE = "SELECT * FROM file_upload a INNER JOIN user_accounts b ON a.account_id = b.account_id "
			+ "LEFT OUTER JOIN students c ON b.account_id = c.account_id "
			+ "LEFT OUTER JOIN professor d ON b.account_id = d.account_id "
			+ "WHERE subject_id = ? AND section = ? ORDER by upload_date DESC";
	
	String INSERT_NEW_SUBJECT = "INSERT into subjects(subject_id, subject_name, subject_time, subject_days, subject_year_level, section, room_number)"
			+ "VALUES (?,?,?,?,?,?,?)";
	
	String INSERT_NEW_CURRICULUM = "INSERT into curriculums(subject_id,course_id,year) VALUES(?,?,?)";
	String CHANGE_USER_PASSWORD = "UPDATE user_accounts SET password = ?, salt = ? WHERE account_id = ?";
	String COUNT_MESSAGE_NOTIFICATION = "SELECT COUNT(status) FROM message_content WHERE status = ? AND receiver_id = ?";
		String UPDATE_MESSAGE_STATUS = "UPDATE message_content SET status = ? WHERE message_id = ?";
		String CHECK_IF_STUDENT_IS_ALREADY_ENROLLED = "SELECT * FROM student_subject_enrollment";
//	String GET_MESSAGE_PARAMS = "SELECT * FROM students INNER JOIN message_content on students.account_id=message_content.sender_id where message_content.receiver_id=?";
//	String GET_MESSAGE_PARAMS_PROF = "SELECT * FROM professor INNER JOIN message_content on professor.account_id=message_content.receiver_id where message_content.receiver_id=?";
	String GET_MESSAGE_PARAMS = "SELECT distinct message_id FROM message_content where receiver_id = ?";
}
