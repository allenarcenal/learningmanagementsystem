package com.lms.model.database;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.lms.model.database.SQL;


public class Database implements SQL{
	private static Connection connection;
	
	public static Connection getConnection(){
		if(connection == null){
		
		try {
		    DataSource dataSource = 
		     (DataSource) InitialContext.doLookup(DB_SOURCE);
		    connection = dataSource.getConnection();
		} catch (NamingException e) {
		    e.printStackTrace();
		} catch (SQLException e) {
		    e.printStackTrace();
		}
		}
		System.out.println("current connection: " + connection.toString());
		return connection;
	}

}
