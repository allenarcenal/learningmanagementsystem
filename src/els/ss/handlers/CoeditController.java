package els.ss.handlers;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zss.api.Importer;
import org.zkoss.zss.api.Importers;
import org.zkoss.zss.api.Ranges;
import org.zkoss.zss.api.model.Book;
import org.zkoss.zss.api.model.Sheet;
import org.zkoss.zss.ui.AuxAction;
import org.zkoss.zss.ui.Spreadsheet;
import org.zkoss.zss.ui.UserActionManager;
import org.zkoss.zss.ui.impl.DefaultUserActionManagerCtrl;
 
public class CoeditController extends SelectorComposer<Component> {
 
    /**
	 * 
	 */
	static private final Map<String,Book> sharedBook = 
	        new HashMap<String,Book>();
	
	private static final long serialVersionUID = 1L;
	@Wire
    private Spreadsheet studentSheet, profSheet;
	public static String filePath;
	public static String studName;
	public static String fileName;
	
   
    public static String getFileName() {
		return fileName;
	}


	public static void setFileName(String fileName) {
		CoeditController.fileName = fileName;
	}


	public static String getStudName() {
		return studName;
	}


	public static void setStudName(String studName) {
		CoeditController.studName = studName;
	}


	public void doAfterCompose(Component comp) throws Exception {
    	System.out.println("Starting the Shit");
        Book book;
        super.doAfterCompose(comp);
        final Importer importer = Importers.getImporter();
        try {
        	System.out.println("MY FILE PATH TO BE SPLITTED: " + filePath);
        	System.out.println(getFilePath());
        	System.out.println("my fp is =" + filePath);
            File file = new File(filePath);
            System.out.println("my fn is =" + getFileName());
        	book = importer.imports(file, getFileName());
           // book = importer.imports(WebApps.getCurrent().getResource("/WEB-INF/books/ron.xlsx"), "ron.xlsx");
        } catch (IOException exception) {
            throw new RuntimeException(exception.getMessage(), exception);
        }
        if (book != null) {
        	book.setShareScope(EventQueues.APPLICATION);

            System.out.println(book.getShareScope());
         // System.out.println(studentSheetheet);
            if(studentSheet != null){
            	System.out.println("my fn is =" + getFileName());
//	            studentSheet.setBook(loadBookFromAvailable(getFileName()));
            	studentSheet.setBook(book);
	            studentSheet.setColumntitles("A");
	            studentSheet.setUserName(studName);
	            Sheet sheet = studentSheet.getSelectedSheet();
	            Ranges.range(sheet).protectSheet("password",
	                    true, true, false, false, false, false, false,
	                    false, false, false, false, false, false, false, false);
            }else if(profSheet != null){
	            profSheet.setBook(book);
	            profSheet.setUserName(studName);
	            System.out.println("USER PROF INCOMING: " + studName);
	            UserActionManager actionManager = profSheet.getUserActionManager();
	            actionManager.registerHandler(
	                    DefaultUserActionManagerCtrl.Category.AUXACTION.getName(),
	                    AuxAction.SAVE_BOOK.getAction(), new SaveBookHandler());
	        }
        }
        
    }
    
  
    public String getFilePath() {
		return filePath;
	}


	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}


	private Book loadBookFromAvailable(String bookname){
        Book book;
        synchronized (sharedBook){
            book = sharedBook.get(bookname);
            if(book==null){
                book = importBook(bookname);
                book.setShareScope("application");
                sharedBook.put(bookname, book);
            }
        }
        return book;
    }
    
     
     
    private Book importBook(String bookname){
        Importer imp = Importers.getImporter();
        try {
            Book book = imp.imports(
                    WebApps.getCurrent().getResource("/WEB-INF/books/" + bookname),
                    bookname);
            return book;
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }
     
    
    

}